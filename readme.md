# INELS CLOUD TEMPLATE

Šablona založená na Foundation for apps s využitím Angularu (1) a Google charts.

## Odkazy
  - [Foundation for apps](http://foundation.zurb.com/apps/docs/#!/)
  - [Google charts](https://developers.google.com/chart/)
  - [Angular](https://angularjs.org/) [Angular docs](https://code.angularjs.org/1.4.12/docs/guide/introduction)
  
## Angular moduly
  - [Angular translate](https://angular-translate.github.io/)
  - [Angular Websocket](https://angularclass.github.io/angular-websocket/)
  - [Angular UI-Router](https://ui-router.github.io/)

## Requirements

You'll need the following software installed to get started.

  - [Node.js](http://nodejs.org): Use the installer for your OS.
  - [Git](http://git-scm.com/downloads): Use the installer for your OS.
    - Windows users can also try [Git for Windows](http://git-for-windows.github.io/).
  - [Gulp](http://gulpjs.com/) and [Bower](http://bower.io): Run `npm install -g gulp bower`
    - Depending on how Node is configured on your machine, you may need to run `sudo npm install -g gulp bower` instead, if you get an error with the first command.

## Get Started

Install the dependencies. If you're running Mac OS or Linux, you may need to run `sudo npm install` instead, depending on how your machine is configured.

```bash
npm install
bower install
```

While you're working on your project, run:

```bash
npm start
```

This will compile the Sass and assemble your Angular app. **Now go to `localhost:8080` in your browser to see it in action.** When you change any file in the `client` folder, the appropriate Gulp task will run to build new files.

To run the compiling process once, without watching any files, use the `build` command.

```bash
npm start build
```

## Directives

### prvky
  - il-header: hlavička
  - il-footer: patička
  - il-status-bar: status bar zařízení
  - il-devices-navigation: navigace c zařízeních
  - il-switch: switcher ("on | off")
  - il-counter: počítadlo ("něco jako input[type=number]")
  - il-colorpicker: výběr barev
  - il-scale: od-do prvek s barevným přechodem pro text
  - il-meter: vertikální stupnice od min do max
  - il-calendar: kalendář
  - il-select: výběr x položek
  - il-hour-chart: tabulkový graf hodnot v jednotlivých hodinách (podbarveným podle hodnoty) ve dnech
  - il-line-chart: čárový graf
  
### třídy a atributy
  - il-toggle (atribut): přepínání tříd odkazovanému prvku
  - il-card (atribut, třída): ovládaní karty
  - color (atribut): změní barvu textu elementu
  - responsive-scroller (atribut, třída): div s posunovačem pro responsivní prvky
