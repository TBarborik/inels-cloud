/*
 AngularJS v1.5.8
 (c) 2010-2016 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(n,c){'use strict';function l(b,a,g){var d=g.baseHref(),k=b[0];return function(b,e,f){var g,h;f=f||{};h=f.expires;g=c.isDefined(f.path)?f.path:d;c.isUndefined(e)&&(h="Thu, 01 Jan 1970 00:00:00 GMT",e="");c.isString(h)&&(h=new Date(h));e=encodeURIComponent(b)+"="+encodeURIComponent(e);e=e+(g?";path="+g:"")+(f.domain?";domain="+f.domain:"");e+=h?";expires="+h.toUTCString():"";e+=f.secure?";secure":"";f=e.length+1;4096<f&&a.warn("Cookie '"+b+"' possibly not set or overflowed because it was too large ("+
f+" > 4096 bytes)!");k.cookie=e}}c.module("ngCookies",["ng"]).provider("$cookies",[function(){var b=this.defaults={};this.$get=["$$cookieReader","$$cookieWriter",function(a,g){return{get:function(d){return a()[d]},getObject:function(d){return(d=this.get(d))?c.fromJson(d):d},getAll:function(){return a()},put:function(d,a,m){g(d,a,m?c.extend({},b,m):b)},putObject:function(d,b,a){this.put(d,c.toJson(b),a)},remove:function(a,k){g(a,void 0,k?c.extend({},b,k):b)}}}]}]);c.module("ngCookies").factory("$cookieStore",
["$cookies",function(b){return{get:function(a){return b.getObject(a)},put:function(a,c){b.putObject(a,c)},remove:function(a){b.remove(a)}}}]);l.$inject=["$document","$log","$browser"];c.module("ngCookies").provider("$$cookieWriter",function(){this.$get=l})})(window,window.angular);
//# sourceMappingURL=angular-cookies.min.js.map

/*!
 * angular-translate - v2.9.0 - 2016-01-24
 * 
 * Copyright (c) 2016 The angular-translate team, Pascal Precht; Licensed MIT
 */
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return b()}):"object"==typeof exports?module.exports=b():b()}(this,function(){function a(a){"use strict";var b=a.storageKey(),c=a.storage(),d=function(){var d=a.preferredLanguage();angular.isString(d)?a.use(d):c.put(b,a.use())};d.displayName="fallbackFromIncorrectStorageValue",c?c.get(b)?a.use(c.get(b))["catch"](d):d():angular.isString(a.preferredLanguage())&&a.use(a.preferredLanguage())}function b(){"use strict";var a,b,c=null,d=!1,e=!1;b={sanitize:function(a,b){return"text"===b&&(a=g(a)),a},escape:function(a,b){return"text"===b&&(a=f(a)),a},sanitizeParameters:function(a,b){return"params"===b&&(a=h(a,g)),a},escapeParameters:function(a,b){return"params"===b&&(a=h(a,f)),a}},b.escaped=b.escapeParameters,this.addStrategy=function(a,c){return b[a]=c,this},this.removeStrategy=function(a){return delete b[a],this},this.useStrategy=function(a){return d=!0,c=a,this},this.$get=["$injector","$log",function(f,g){var h={},i=function(a,c,d){return angular.forEach(d,function(d){if(angular.isFunction(d))a=d(a,c);else if(angular.isFunction(b[d]))a=b[d](a,c);else{if(!angular.isString(b[d]))throw new Error("pascalprecht.translate.$translateSanitization: Unknown sanitization strategy: '"+d+"'");if(!h[b[d]])try{h[b[d]]=f.get(b[d])}catch(e){throw h[b[d]]=function(){},new Error("pascalprecht.translate.$translateSanitization: Unknown sanitization strategy: '"+d+"'")}a=h[b[d]](a,c)}}),a},j=function(){d||e||(g.warn("pascalprecht.translate.$translateSanitization: No sanitization strategy has been configured. This can have serious security implications. See http://angular-translate.github.io/docs/#/guide/19_security for details."),e=!0)};return f.has("$sanitize")&&(a=f.get("$sanitize")),{useStrategy:function(a){return function(b){a.useStrategy(b)}}(this),sanitize:function(a,b,d){if(c||j(),arguments.length<3&&(d=c),!d)return a;var e=angular.isArray(d)?d:[d];return i(a,b,e)}}}];var f=function(a){var b=angular.element("<div></div>");return b.text(a),b.html()},g=function(b){if(!a)throw new Error("pascalprecht.translate.$translateSanitization: Error cannot find $sanitize service. Either include the ngSanitize module (https://docs.angularjs.org/api/ngSanitize) or use a sanitization strategy which does not depend on $sanitize, such as 'escape'.");return a(b)},h=function(a,b){if(angular.isObject(a)){var c=angular.isArray(a)?[]:{};return angular.forEach(a,function(a,d){c[d]=h(a,b)}),c}return angular.isNumber(a)?a:b(a)}}function c(a,b,c,d){"use strict";var e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t={},u=[],v=a,w=[],x="translate-cloak",y=!1,z=!1,A=".",B=!1,C=0,D=!0,E="default",F={"default":function(a){return(a||"").split("-").join("_")},java:function(a){var b=(a||"").split("-").join("_"),c=b.split("_");return c.length>1?c[0].toLowerCase()+"_"+c[1].toUpperCase():b},bcp47:function(a){var b=(a||"").split("_").join("-"),c=b.split("-");return c.length>1?c[0].toLowerCase()+"-"+c[1].toUpperCase():b}},G="2.9.0",H=function(){if(angular.isFunction(d.getLocale))return d.getLocale();var a,c,e=b.$get().navigator,f=["language","browserLanguage","systemLanguage","userLanguage"];if(angular.isArray(e.languages))for(a=0;a<e.languages.length;a++)if(c=e.languages[a],c&&c.length)return c;for(a=0;a<f.length;a++)if(c=e[f[a]],c&&c.length)return c;return null};H.displayName="angular-translate/service: getFirstBrowserLanguage";var I=function(){var a=H()||"";return F[E]&&(a=F[E](a)),a};I.displayName="angular-translate/service: getLocale";var J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K=function(){return this.toString().replace(/^\s+|\s+$/g,"")},L=function(a){if(a){for(var b=[],c=angular.lowercase(a),d=0,e=u.length;e>d;d++)b.push(angular.lowercase(u[d]));if(J(b,c)>-1)return a;if(f){var g;for(var h in f){var i=!1,j=Object.prototype.hasOwnProperty.call(f,h)&&angular.lowercase(h)===angular.lowercase(a);if("*"===h.slice(-1)&&(i=h.slice(0,-1)===a.slice(0,h.length-1)),(j||i)&&(g=f[h],J(b,angular.lowercase(g))>-1))return g}}var k=a.split("_");return k.length>1&&J(b,angular.lowercase(k[0]))>-1?k[0]:void 0}},M=function(a,b){if(!a&&!b)return t;if(a&&!b){if(angular.isString(a))return t[a]}else angular.isObject(t[a])||(t[a]={}),angular.extend(t[a],N(b));return this};this.translations=M,this.cloakClassName=function(a){return a?(x=a,this):x},this.nestedObjectDelimeter=function(a){return a?(A=a,this):A};var N=function(a,b,c,d){var e,f,g,h;b||(b=[]),c||(c={});for(e in a)Object.prototype.hasOwnProperty.call(a,e)&&(h=a[e],angular.isObject(h)?N(h,b.concat(e),c,e):(f=b.length?""+b.join(A)+A+e:e,b.length&&e===d&&(g=""+b.join(A),c[g]="@:"+f),c[f]=h));return c};N.displayName="flatObject",this.addInterpolation=function(a){return w.push(a),this},this.useMessageFormatInterpolation=function(){return this.useInterpolation("$translateMessageFormatInterpolation")},this.useInterpolation=function(a){return n=a,this},this.useSanitizeValueStrategy=function(a){return c.useStrategy(a),this},this.preferredLanguage=function(a){return a?(O(a),this):e};var O=function(a){return a&&(e=a),e};this.translationNotFoundIndicator=function(a){return this.translationNotFoundIndicatorLeft(a),this.translationNotFoundIndicatorRight(a),this},this.translationNotFoundIndicatorLeft=function(a){return a?(q=a,this):q},this.translationNotFoundIndicatorRight=function(a){return a?(r=a,this):r},this.fallbackLanguage=function(a){return P(a),this};var P=function(a){return a?(angular.isString(a)?(h=!0,g=[a]):angular.isArray(a)&&(h=!1,g=a),angular.isString(e)&&J(g,e)<0&&g.push(e),this):h?g[0]:g};this.use=function(a){if(a){if(!t[a]&&!o)throw new Error("$translateProvider couldn't find translationTable for langKey: '"+a+"'");return i=a,this}return i};var Q=function(a){return a?(v=a,this):l?l+v:v};this.storageKey=Q,this.useUrlLoader=function(a,b){return this.useLoader("$translateUrlLoader",angular.extend({url:a},b))},this.useStaticFilesLoader=function(a){return this.useLoader("$translateStaticFilesLoader",a)},this.useLoader=function(a,b){return o=a,p=b||{},this},this.useLocalStorage=function(){return this.useStorage("$translateLocalStorage")},this.useCookieStorage=function(){return this.useStorage("$translateCookieStorage")},this.useStorage=function(a){return k=a,this},this.storagePrefix=function(a){return a?(l=a,this):a},this.useMissingTranslationHandlerLog=function(){return this.useMissingTranslationHandler("$translateMissingTranslationHandlerLog")},this.useMissingTranslationHandler=function(a){return m=a,this},this.usePostCompiling=function(a){return y=!!a,this},this.forceAsyncReload=function(a){return z=!!a,this},this.uniformLanguageTag=function(a){return a?angular.isString(a)&&(a={standard:a}):a={},E=a.standard,this},this.determinePreferredLanguage=function(a){var b=a&&angular.isFunction(a)?a():I();return e=u.length?L(b)||b:b,this},this.registerAvailableLanguageKeys=function(a,b){return a?(u=a,b&&(f=b),this):u},this.useLoaderCache=function(a){return a===!1?s=void 0:a===!0?s=!0:"undefined"==typeof a?s="$translationCache":a&&(s=a),this},this.directivePriority=function(a){return void 0===a?C:(C=a,this)},this.statefulFilter=function(a){return void 0===a?D:(D=a,this)},this.$get=["$log","$injector","$rootScope","$q",function(a,b,c,d){var f,l,u,E=b.get(n||"$translateDefaultInterpolation"),F=!1,H={},I={},R=function(a,b,c,h,j){var m=j&&j!==i?L(j)||j:i;if(angular.isArray(a)){var n=function(a){for(var e={},f=[],g=function(a){var f=d.defer(),g=function(b){e[a]=b,f.resolve([a,b])};return R(a,b,c,h,j).then(g,g),f.promise},i=0,k=a.length;k>i;i++)f.push(g(a[i]));return d.all(f).then(function(){return e})};return n(a)}var o=d.defer();a&&(a=K.apply(a));var p=function(){var a=e?I[e]:I[m];if(l=0,k&&!a){var b=f.get(v);if(a=I[b],g&&g.length){var c=J(g,b);l=0===c?1:0,J(g,e)<0&&g.push(e)}}return a}();if(p){var q=function(){j||(m=i),ca(a,b,c,h,m).then(o.resolve,o.reject)};q.displayName="promiseResolved",p["finally"](q,o.reject)}else ca(a,b,c,h,m).then(o.resolve,o.reject);return o.promise},S=function(a){return q&&(a=[q,a].join(" ")),r&&(a=[a,r].join(" ")),a},T=function(a){i=a,k&&f.put(R.storageKey(),i),c.$emit("$translateChangeSuccess",{language:a}),E.setLocale(i);var b=function(a,b){H[b].setLocale(i)};b.displayName="eachInterpolatorLocaleSetter",angular.forEach(H,b),c.$emit("$translateChangeEnd",{language:a})},U=function(a){if(!a)throw"No language key specified for loading.";var e=d.defer();c.$emit("$translateLoadingStart",{language:a}),F=!0;var f=s;"string"==typeof f&&(f=b.get(f));var g=angular.extend({},p,{key:a,$http:angular.extend({},{cache:f},p.$http)}),h=function(b){var d={};c.$emit("$translateLoadingSuccess",{language:a}),angular.isArray(b)?angular.forEach(b,function(a){angular.extend(d,N(a))}):angular.extend(d,N(b)),F=!1,e.resolve({key:a,table:d}),c.$emit("$translateLoadingEnd",{language:a})};h.displayName="onLoaderSuccess";var i=function(a){c.$emit("$translateLoadingError",{language:a}),e.reject(a),c.$emit("$translateLoadingEnd",{language:a})};return i.displayName="onLoaderError",b.get(o)(g).then(h,i),e.promise};if(k&&(f=b.get(k),!f.get||!f.put))throw new Error("Couldn't use storage '"+k+"', missing get() or put() method!");if(w.length){var V=function(a){var c=b.get(a);c.setLocale(e||i),H[c.getInterpolationIdentifier()]=c};V.displayName="interpolationFactoryAdder",angular.forEach(w,V)}var W=function(a){var b=d.defer();if(Object.prototype.hasOwnProperty.call(t,a))b.resolve(t[a]);else if(I[a]){var c=function(a){M(a.key,a.table),b.resolve(a.table)};c.displayName="translationTableResolver",I[a].then(c,b.reject)}else b.reject();return b.promise},X=function(a,b,c,e){var f=d.defer(),g=function(d){if(Object.prototype.hasOwnProperty.call(d,b)){e.setLocale(a);var g=d[b];"@:"===g.substr(0,2)?X(a,g.substr(2),c,e).then(f.resolve,f.reject):f.resolve(e.interpolate(d[b],c)),e.setLocale(i)}else f.reject()};return g.displayName="fallbackTranslationResolver",W(a).then(g,f.reject),f.promise},Y=function(a,b,c,d){var e,f=t[a];if(f&&Object.prototype.hasOwnProperty.call(f,b)){if(d.setLocale(a),e=d.interpolate(f[b],c),"@:"===e.substr(0,2))return Y(a,e.substr(2),c,d);d.setLocale(i)}return e},Z=function(a,c){if(m){var d=b.get(m)(a,i,c);return void 0!==d?d:a}return a},$=function(a,b,c,e,f){var h=d.defer();if(a<g.length){var i=g[a];X(i,b,c,e).then(h.resolve,function(){$(a+1,b,c,e,f).then(h.resolve)})}else f?h.resolve(f):h.resolve(Z(b,c));return h.promise},_=function(a,b,c,d){var e;if(a<g.length){var f=g[a];e=Y(f,b,c,d),e||(e=_(a+1,b,c,d))}return e},aa=function(a,b,c,d){return $(u>0?u:l,a,b,c,d)},ba=function(a,b,c){return _(u>0?u:l,a,b,c)},ca=function(a,b,c,e,f){var h=d.defer(),i=f?t[f]:t,j=c?H[c]:E;if(i&&Object.prototype.hasOwnProperty.call(i,a)){var k=i[a];"@:"===k.substr(0,2)?R(k.substr(2),b,c,e,f).then(h.resolve,h.reject):h.resolve(j.interpolate(k,b))}else{var l;m&&!F&&(l=Z(a,b)),f&&g&&g.length?aa(a,b,j,e).then(function(a){h.resolve(a)},function(a){h.reject(S(a))}):m&&!F&&l?e?h.resolve(e):h.resolve(l):e?h.resolve(e):h.reject(S(a))}return h.promise},da=function(a,b,c,d){var e,f=d?t[d]:t,h=E;if(H&&Object.prototype.hasOwnProperty.call(H,c)&&(h=H[c]),f&&Object.prototype.hasOwnProperty.call(f,a)){var i=f[a];e="@:"===i.substr(0,2)?da(i.substr(2),b,c,d):h.interpolate(i,b)}else{var j;m&&!F&&(j=Z(a,b)),d&&g&&g.length?(l=0,e=ba(a,b,h)):e=m&&!F&&j?j:S(a)}return e},ea=function(a){j===a&&(j=void 0),I[a]=void 0};R.preferredLanguage=function(a){return a&&O(a),e},R.cloakClassName=function(){return x},R.nestedObjectDelimeter=function(){return A},R.fallbackLanguage=function(a){if(void 0!==a&&null!==a){if(P(a),o&&g&&g.length)for(var b=0,c=g.length;c>b;b++)I[g[b]]||(I[g[b]]=U(g[b]));R.use(R.use())}return h?g[0]:g},R.useFallbackLanguage=function(a){if(void 0!==a&&null!==a)if(a){var b=J(g,a);b>-1&&(u=b)}else u=0},R.proposedLanguage=function(){return j},R.storage=function(){return f},R.negotiateLocale=L,R.use=function(a){if(!a)return i;var b=d.defer();c.$emit("$translateChangeStart",{language:a});var e=L(a);return e&&(a=e),!z&&t[a]||!o||I[a]?j===a&&I[a]?I[a].then(function(a){return b.resolve(a.key),a},function(a){return b.reject(a),d.reject(a)}):(b.resolve(a),T(a)):(j=a,I[a]=U(a).then(function(c){return M(c.key,c.table),b.resolve(c.key),j===a&&T(c.key),c},function(a){return c.$emit("$translateChangeError",{language:a}),b.reject(a),c.$emit("$translateChangeEnd",{language:a}),d.reject(a)}),I[a]["finally"](function(){ea(a)})),b.promise},R.storageKey=function(){return Q()},R.isPostCompilingEnabled=function(){return y},R.isForceAsyncReloadEnabled=function(){return z},R.refresh=function(a){function b(){f.resolve(),c.$emit("$translateRefreshEnd",{language:a})}function e(){f.reject(),c.$emit("$translateRefreshEnd",{language:a})}if(!o)throw new Error("Couldn't refresh translation table, no loader registered!");var f=d.defer();if(c.$emit("$translateRefreshStart",{language:a}),a)if(t[a]){var h=function(c){M(c.key,c.table),a===i&&T(i),b()};h.displayName="refreshPostProcessor",U(a).then(h,e)}else e();else{var j=[],k={};if(g&&g.length)for(var l=0,m=g.length;m>l;l++)j.push(U(g[l])),k[g[l]]=!0;i&&!k[i]&&j.push(U(i));var n=function(a){t={},angular.forEach(a,function(a){M(a.key,a.table)}),i&&T(i),b()};n.displayName="refreshPostProcessor",d.all(j).then(n,e)}return f.promise},R.instant=function(a,b,c,d){var f=d&&d!==i?L(d)||d:i;if(null===a||angular.isUndefined(a))return a;if(angular.isArray(a)){for(var h={},j=0,k=a.length;k>j;j++)h[a[j]]=R.instant(a[j],b,c,d);return h}if(angular.isString(a)&&a.length<1)return a;a&&(a=K.apply(a));var l,n=[];e&&n.push(e),f&&n.push(f),g&&g.length&&(n=n.concat(g));for(var o=0,p=n.length;p>o;o++){var s=n[o];if(t[s]&&"undefined"!=typeof t[s][a]&&(l=da(a,b,c,f)),"undefined"!=typeof l)break}return l||""===l||(q||r?l=S(a):(l=E.interpolate(a,b),m&&!F&&(l=Z(a,b)))),l},R.versionInfo=function(){return G},R.loaderCache=function(){return s},R.directivePriority=function(){return C},R.statefulFilter=function(){return D},R.isReady=function(){return B};var fa=d.defer();fa.promise.then(function(){B=!0}),R.onReady=function(a){var b=d.defer();return angular.isFunction(a)&&b.promise.then(a),B?b.resolve():fa.promise.then(b.resolve),b.promise};var ga=c.$on("$translateReady",function(){fa.resolve(),ga(),ga=null}),ha=c.$on("$translateChangeEnd",function(){fa.resolve(),ha(),ha=null});if(o){if(angular.equals(t,{})&&R.use()&&R.use(R.use()),g&&g.length)for(var ia=function(a){return M(a.key,a.table),c.$emit("$translateChangeEnd",{language:a.key}),a},ja=0,ka=g.length;ka>ja;ja++){var la=g[ja];(z||!t[la])&&(I[la]=U(la).then(ia))}}else c.$emit("$translateReady",{language:R.use()});return R}]}function d(a,b){"use strict";var c,d={},e="default";return d.setLocale=function(a){c=a},d.getInterpolationIdentifier=function(){return e},d.useSanitizeValueStrategy=function(a){return b.useStrategy(a),this},d.interpolate=function(c,d){d=d||{},d=b.sanitize(d,"params");var e=a(c)(d);return e=b.sanitize(e,"text")},d}function e(a,b,c,d,e,g){"use strict";var h=function(){return this.toString().replace(/^\s+|\s+$/g,"")};return{restrict:"AE",scope:!0,priority:a.directivePriority(),compile:function(b,i){var j=i.translateValues?i.translateValues:void 0,k=i.translateInterpolation?i.translateInterpolation:void 0,l=b[0].outerHTML.match(/translate-value-+/i),m="^(.*)("+c.startSymbol()+".*"+c.endSymbol()+")(.*)",n="^(.*)"+c.startSymbol()+"(.*)"+c.endSymbol()+"(.*)";return function(b,o,p){b.interpolateParams={},b.preText="",b.postText="",b.translateNamespace=f(b);var q={},r=function(a,c,d){if(c.translateValues&&angular.extend(a,e(c.translateValues)(b.$parent)),l)for(var f in d)if(Object.prototype.hasOwnProperty.call(c,f)&&"translateValue"===f.substr(0,14)&&"translateValues"!==f){var g=angular.lowercase(f.substr(14,1))+f.substr(15);a[g]=d[f]}},s=function(a){if(angular.isFunction(s._unwatchOld)&&(s._unwatchOld(),s._unwatchOld=void 0),angular.equals(a,"")||!angular.isDefined(a)){var d=h.apply(o.text()),e=d.match(m);if(angular.isArray(e)){b.preText=e[1],b.postText=e[3],q.translate=c(e[2])(b.$parent);var f=d.match(n);angular.isArray(f)&&f[2]&&f[2].length&&(s._unwatchOld=b.$watch(f[2],function(a){q.translate=a,y()}))}else q.translate=d?d:void 0}else q.translate=a;y()},t=function(a){p.$observe(a,function(b){q[a]=b,y()})};r(b.interpolateParams,p,i);var u=!0;p.$observe("translate",function(a){"undefined"==typeof a?s(""):""===a&&u||(q.translate=a,y()),u=!1});for(var v in p)p.hasOwnProperty(v)&&"translateAttr"===v.substr(0,13)&&t(v);if(p.$observe("translateDefault",function(a){b.defaultText=a,y()}),j&&p.$observe("translateValues",function(a){a&&b.$parent.$watch(function(){angular.extend(b.interpolateParams,e(a)(b.$parent))})}),l){var w=function(a){p.$observe(a,function(c){var d=angular.lowercase(a.substr(14,1))+a.substr(15);b.interpolateParams[d]=c})};for(var x in p)Object.prototype.hasOwnProperty.call(p,x)&&"translateValue"===x.substr(0,14)&&"translateValues"!==x&&w(x)}var y=function(){for(var a in q)q.hasOwnProperty(a)&&void 0!==q[a]&&z(a,q[a],b,b.interpolateParams,b.defaultText,b.translateNamespace)},z=function(b,c,d,e,f,g){c?(g&&"."===c.charAt(0)&&(c=g+c),a(c,e,k,f,d.translateLanguage).then(function(a){A(a,d,!0,b)},function(a){A(a,d,!1,b)})):A(c,d,!1,b)},A=function(b,c,e,f){if("translate"===f){e||"undefined"==typeof c.defaultText||(b=c.defaultText),o.empty().append(c.preText+b+c.postText);var g=a.isPostCompilingEnabled(),h="undefined"!=typeof i.translateCompile,j=h&&"false"!==i.translateCompile;(g&&!h||j)&&d(o.contents())(c)}else{e||"undefined"==typeof c.defaultText||(b=c.defaultText);var k=p.$attr[f];"data-"===k.substr(0,5)&&(k=k.substr(5)),k=k.substr(15),o.attr(k,b)}};(j||l||p.translateDefault)&&b.$watch("interpolateParams",y,!0),b.$watch("translateLanguage",y);var B=g.$on("$translateChangeSuccess",y);o.text().length?s(p.translate?p.translate:""):p.translate&&s(p.translate),y(),b.$on("$destroy",B)}}}}function f(a){"use strict";return a.translateNamespace?a.translateNamespace:a.$parent?f(a.$parent):void 0}function g(a,b){"use strict";return{compile:function(c){var d=function(){c.addClass(a.cloakClassName())},e=function(){c.removeClass(a.cloakClassName())};return a.onReady(function(){e()}),d(),function(c,f,g){g.translateCloak&&g.translateCloak.length&&(g.$observe("translateCloak",function(b){a(b).then(e,d)}),b.$on("$translateChangeSuccess",function(){a(g.translateCloak).then(e,d)}))}}}}function h(){"use strict";return{restrict:"A",scope:!0,compile:function(){return{pre:function(a,b,c){a.translateNamespace=f(a),a.translateNamespace&&"."===c.translateNamespace.charAt(0)?a.translateNamespace+=c.translateNamespace:a.translateNamespace=c.translateNamespace}}}}}function f(a){"use strict";return a.translateNamespace?a.translateNamespace:a.$parent?f(a.$parent):void 0}function i(){"use strict";return{restrict:"A",scope:!0,compile:function(){return function(a,b,c){c.$observe("translateLanguage",function(b){a.translateLanguage=b})}}}}function j(a,b){"use strict";var c=function(c,d,e,f){return angular.isObject(d)||(d=a(d)(this)),b.instant(c,d,e,f)};return b.statefulFilter()&&(c.$stateful=!0),c}function k(a){"use strict";return a("translations")}return angular.module("pascalprecht.translate",["ng"]).run(a),a.$inject=["$translate"],a.displayName="runTranslate",angular.module("pascalprecht.translate").provider("$translateSanitization",b),angular.module("pascalprecht.translate").constant("pascalprechtTranslateOverrider",{}).provider("$translate",c),c.$inject=["$STORAGE_KEY","$windowProvider","$translateSanitizationProvider","pascalprechtTranslateOverrider"],c.displayName="displayName",angular.module("pascalprecht.translate").factory("$translateDefaultInterpolation",d),d.$inject=["$interpolate","$translateSanitization"],d.displayName="$translateDefaultInterpolation",angular.module("pascalprecht.translate").constant("$STORAGE_KEY","NG_TRANSLATE_LANG_KEY"),angular.module("pascalprecht.translate").directive("translate",e),e.$inject=["$translate","$q","$interpolate","$compile","$parse","$rootScope"],e.displayName="translateDirective",angular.module("pascalprecht.translate").directive("translateCloak",g),g.$inject=["$translate","$rootScope"],g.displayName="translateCloakDirective",angular.module("pascalprecht.translate").directive("translateNamespace",h),h.displayName="translateNamespaceDirective",angular.module("pascalprecht.translate").directive("translateLanguage",i),i.displayName="translateLanguageDirective",angular.module("pascalprecht.translate").filter("translate",j),j.$inject=["$parse","$translate"],j.displayName="translateFilterFactory",angular.module("pascalprecht.translate").factory("$translationCache",k),k.$inject=["$cacheFactory"],k.displayName="$translationCache","pascalprecht.translate"});
!function(e,t){if("function"==typeof define&&define.amd)define(["module","exports","angular","ws"],t);else if("undefined"!=typeof exports)t(module,exports,require("angular"),require("ws"));else{var o={exports:{}};t(o,o.exports,e.angular,e.ws),e.angularWebsocket=o.exports}}(this,function(e,t,o,n){"use strict";function s(e){return e&&e.__esModule?e:{"default":e}}function r(e,t,o,n){function s(t,o,n){n||!k(o)||C(o)||(n=o,o=void 0),this.protocols=o,this.url=t||"Missing URL",this.ssl=/(wss)/i.test(this.url),this.scope=n&&n.scope||e,this.rootScopeFailover=n&&n.rootScopeFailover&&!0,this.useApplyAsync=n&&n.useApplyAsync||!1,this.initialTimeout=n&&n.initialTimeout||500,this.maxTimeout=n&&n.maxTimeout||3e5,this.reconnectIfNotNormalClose=n&&n.reconnectIfNotNormalClose||!1,this.binaryType=n&&n.binaryType||"blob",this._reconnectAttempts=0,this.sendQueue=[],this.onOpenCallbacks=[],this.onMessageCallbacks=[],this.onErrorCallbacks=[],this.onCloseCallbacks=[],f(this._readyStateConstants),t?this._connect():this._setInternalState(0)}return s.prototype._readyStateConstants={CONNECTING:0,OPEN:1,CLOSING:2,CLOSED:3,RECONNECT_ABORTED:4},s.prototype._normalCloseCode=1e3,s.prototype._reconnectableStatusCodes=[4e3],s.prototype.safeDigest=function(e){e&&!this.scope.$$phase&&this.scope.$digest()},s.prototype.bindToScope=function(t){var o=this;return t&&(this.scope=t,this.rootScopeFailover&&this.scope.$on("$destroy",function(){o.scope=e})),o},s.prototype._connect=function(e){!e&&this.socket&&this.socket.readyState===this._readyStateConstants.OPEN||(this.socket=n.create(this.url,this.protocols),this.socket.onmessage=c["default"].bind(this,this._onMessageHandler),this.socket.onopen=c["default"].bind(this,this._onOpenHandler),this.socket.onerror=c["default"].bind(this,this._onErrorHandler),this.socket.onclose=c["default"].bind(this,this._onCloseHandler),this.socket.binaryType=this.binaryType)},s.prototype.fireQueue=function(){for(;this.sendQueue.length&&this.socket.readyState===this._readyStateConstants.OPEN;){var e=this.sendQueue.shift();this.socket.send(d(e.message)||"blob"!=this.binaryType?e.message:JSON.stringify(e.message)),e.deferred.resolve()}},s.prototype.notifyOpenCallbacks=function(e){for(var t=0;t<this.onOpenCallbacks.length;t++)this.onOpenCallbacks[t].call(this,e)},s.prototype.notifyCloseCallbacks=function(e){for(var t=0;t<this.onCloseCallbacks.length;t++)this.onCloseCallbacks[t].call(this,e)},s.prototype.notifyErrorCallbacks=function(e){for(var t=0;t<this.onErrorCallbacks.length;t++)this.onErrorCallbacks[t].call(this,e)},s.prototype.onOpen=function(e){return this.onOpenCallbacks.push(e),this},s.prototype.onClose=function(e){return this.onCloseCallbacks.push(e),this},s.prototype.onError=function(e){return this.onErrorCallbacks.push(e),this},s.prototype.onMessage=function(e,t){if(!y(e))throw new Error("Callback must be a function");if(t&&b(t.filter)&&!d(t.filter)&&!(t.filter instanceof RegExp))throw new Error("Pattern must be a string or regular expression");return this.onMessageCallbacks.push({fn:e,pattern:t?t.filter:void 0,autoApply:t?t.autoApply:!0}),this},s.prototype._onOpenHandler=function(e){this._reconnectAttempts=0,this.notifyOpenCallbacks(e),this.fireQueue()},s.prototype._onCloseHandler=function(e){var t=this;t.useApplyAsync?t.scope.$applyAsync(function(){t.notifyCloseCallbacks(e)}):(t.notifyCloseCallbacks(e),t.safeDigest(!0)),(this.reconnectIfNotNormalClose&&e.code!==this._normalCloseCode||this._reconnectableStatusCodes.indexOf(e.code)>-1)&&this.reconnect()},s.prototype._onErrorHandler=function(e){var t=this;t.useApplyAsync?t.scope.$applyAsync(function(){t.notifyErrorCallbacks(e)}):(t.notifyErrorCallbacks(e),t.safeDigest(!0))},s.prototype._onMessageHandler=function(e){function t(e,t,o){o=m.call(arguments,2),s.useApplyAsync?s.scope.$applyAsync(function(){e.apply(s,o)}):(e.apply(s,o),s.safeDigest(t))}for(var o,n,s=this,r=0;r<s.onMessageCallbacks.length;r++)n=s.onMessageCallbacks[r],o=n.pattern,o?d(o)&&e.data===o?t(n.fn,n.autoApply,e):o instanceof RegExp&&o.exec(e.data)&&t(n.fn,n.autoApply,e):t(n.fn,n.autoApply,e)},s.prototype.close=function(e){return!e&&this.socket.bufferedAmount||this.socket.close(),this},s.prototype.send=function(e){function o(e){e.cancel=s;var t=e.then;return e.then=function(){var e=t.apply(this,arguments);return o(e)},e}function s(t){return i.sendQueue.splice(i.sendQueue.indexOf(e),1),r.reject(t),i}var r=t.defer(),i=this,a=o(r.promise);return i.readyState===i._readyStateConstants.RECONNECT_ABORTED?r.reject("Socket connection has been closed"):(i.sendQueue.push({message:e,deferred:r}),i.fireQueue()),n.isMocked&&n.isMocked()&&n.isConnected(this.url)&&this._onMessageHandler(n.mockSend()),a},s.prototype.reconnect=function(){this.close();var e=this._getBackoffDelay(++this._reconnectAttempts),t=e/1e3;return console.log("Reconnecting in "+t+" seconds"),o(c["default"].bind(this,this._connect),e),this},s.prototype._getBackoffDelay=function(e){var t=Math.random()+1,o=this.initialTimeout,n=2,s=e,r=this.maxTimeout;return Math.floor(Math.min(t*o*Math.pow(n,s),r))},s.prototype._setInternalState=function(e){if(Math.floor(e)!==e||0>e||e>4)throw new Error("state must be an integer between 0 and 4, got: "+e);h||(this.readyState=e||this.socket.readyState),this._internalConnectionState=e,g(this.sendQueue,function(e){e.deferred.reject("Message cancelled due to closed socket connection")})},h&&h(s.prototype,"readyState",{get:function(){return this._internalConnectionState||this.socket.readyState},set:function(){throw new Error("The readyState property is read-only")}}),function(e,t,o){return new s(e,t,o)}}function i(e){this.create=function(e,t){var o=/wss?:\/\//.exec(e);if(!o)throw new Error("Invalid url provided");return t?new a(e,t):new a(e)},this.createWebSocketBackend=function(t,o){return e.warn("Deprecated: Please use .create(url, protocols)"),this.create(t,o)}}Object.defineProperty(t,"__esModule",{value:!0});var a,c=s(o),l="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol?"symbol":typeof e};if("object"===("undefined"==typeof t?"undefined":l(t))&&"function"==typeof require)try{a=n.Client||n.client||n}catch(u){}a=a||window.WebSocket||window.MozWebSocket;var p=c["default"].noop,f=Object.freeze?Object.freeze:p,h=Object.defineProperty,d=c["default"].isString,y=c["default"].isFunction,b=c["default"].isDefined,k=c["default"].isObject,C=c["default"].isArray,g=c["default"].forEach,m=Array.prototype.slice;Array.prototype.indexOf||(Array.prototype.indexOf=function(e){var t=this.length>>>0,o=Number(arguments[1])||0;for(o=0>o?Math.ceil(o):Math.floor(o),0>o&&(o+=t);t>o;o++)if(o in this&&this[o]===e)return o;return-1}),c["default"].module("ngWebSocket",[]).factory("$websocket",["$rootScope","$q","$timeout","$websocketBackend",r]).factory("WebSocket",["$rootScope","$q","$timeout","WebsocketBackend",r]).service("$websocketBackend",["$log",i]).service("WebSocketBackend",["$log",i]),c["default"].module("angular-websocket",["ngWebSocket"]),t["default"]=c["default"].module("ngWebSocket"),e.exports=t["default"]});
//# sourceMappingURL=angular-websocket.min.js.map

angular.module("Authentication", ["Api", "ngCookies"]).factory("AuthService", ['ApiService', '$cookies', "$state", function (ApiService, $cookies, $state) {
  var AuthService = {};

  AuthService.isLoggedIn = function () {
    var is = typeof $cookies.get("TWISTED_SESSION") != "undefined" || (typeof(Storage) !== "undefined" && typeof(localStorage.loggedIn) !== "undefined" && localStorage.loggedIn);
    if (!is && typeof Storage !== "undefined" && typeof localStorage.username !== "undefined")
      localStorage.removeItem("username");

    return is;
  };

  AuthService.logIn = function (name, password, success, fail) {
    ApiService.setApi(ApiService.APIv1);
    ApiService.post("login", {username: name, password: password},
      function (response) {
        if (response.status == 200) {
          if (typeof(Storage) !== "undefined") {
            localStorage.username = name;
            localStorage.loggedIn = true;
          }
          success();
        }
        else
          fail();
      },
      function (response) {
        fail();
      });
    ApiService.setApi(ApiService.APIv2);
  };

  AuthService.logOut = function (success, fail) {
    ApiService.setApi(ApiService.APIv1);
    ApiService.get("signout", {}, function (response) {
      $cookies.remove("TWISTED_SESSION");
      if (typeof(Storage) !== "undefined") {
        localStorage.removeItem("username");
        localStorage.removeItem("loggedIn");
      }
      window.location = "/";
      success();
    }, function (response) {
      fail();
    });
    ApiService.setApi(ApiService.APIv2);
  };

  AuthService.getUser = function () {
    var user = {
      username: ""
    };

    if (typeof(Storage) !== "undefined" && typeof(localStorage.username) !== "undefined") {
      user.username = localStorage.username;
    }

    return user;
  };

  return AuthService;
}]);

angular.module("Api", ['ngCookies']).factory("ApiService", ['$http', '$cookies', function ($http, $cookies) {
  var server = "https://www.elkoep.cloud/api/v1";
  var ApiService = {};

  ApiService.versions = [
    "https://www.elkoep.cloud/api/v1",
    "https://www.elkoep.cloud/api/v2"
  ];

  ApiService.APIv1 = 0;
  ApiService.APIv2 = 1;
  ApiService.currentApi = ApiService.versions[ApiService.APIv2]; // default

  ApiService.setApi = function(v) {
    ApiService.currentApi = ApiService.versions[v];
  };

  ApiService.request = function (method, endpoint, data, successFn, errorFn) {

    var vars = {
      method: method,
      url: ApiService.currentApi + '/' + endpoint,
      data: data
    };

    if (typeof successFn != "undefined") {
      if (typeof errorFn == "undefined") {
        $http(vars).then(successFn);
      } else {
        $http(vars).then(successFn, function (r) {
            errorFn(r)
          }
        );
      }
    } else {
      $http(vars);
    }
  };

  ApiService.post = function (endpoint, data, successFn, failFn) {
    ApiService.request("POST", endpoint, data, successFn, failFn);
  };

  ApiService.get = function (endpoint, data, successFn, failFn) {
    if (typeof data === 'function') {
      successFn = data;
      failFn = successFn;
      data = {};
    }

    ApiService.request("GET", endpoint, data, successFn, failFn);
  };

  ApiService.put = function (endpoint, data, successFn, failFn) {
    if (typeof data === 'function') {
      successFn = data;
      failFn = successFn;
      data = {};
    }

    ApiService.request("PUT", endpoint, data, successFn, failFn);
  };

  ApiService.delete = function (endpoint, data, successFn, failFn) {
    ApiService.request("DELETE", endpoint, data, successFn, failFn);
  };

  return ApiService;
}]);

angular.module("Rooms", ["Api"]).factory("RoomService", ['ApiService', function (ApiService) {
  var RoomService = {};

  RoomService.devices = [
    'rgba',
    'light_switch',
    'light_dimmer',
    'blinds'
  ];

  RoomService.actual = 0;

  RoomService.getRooms = function (success, fail) {
    RoomService.getMac(RoomService.actual, function (mac) {
      ApiService.get("rooms/" + mac, function (r) {
        success(r, mac)
      }, fail);
    });
  };

  RoomService.get = function (findmac, room, success, fail) {
    if (findmac) {
      RoomService.getMac(RoomService.actual, function (mac) {
        ApiService.get("rooms/" + mac + "/" + room, function (r) {
          var data = [];
          for (var deviceType in r.data.dev_type) {
            for (var deviceOrder in r.data.dev_type[deviceType]) {
              var device = r.data.dev_type[deviceType][deviceOrder];
              device.order = parseInt(deviceOrder);
              device.type = deviceType;
              var roomName = room.split('/').pop();
              device.room = roomName;
              if (eval("typeof(RoomService." + device.type + "_in_edit)") === "function") {
                device = eval("RoomService." + device.type + "_in_edit(device)");
              }

              data.push(device);
            }
          }
          success(data, room);
        }, fail);
      });
    } else {
      ApiService.get("rooms/" + room, function (r) {
        var data = [];
        var device = {};
        for (var deviceType in r.data.dev_type) {
          for (var deviceOrder in r.data.dev_type[deviceType]) {
            device = {};
            device = r.data.dev_type[deviceType][deviceOrder];
            device.order = parseInt(deviceOrder);
            device.type = deviceType;
            var roomName = room.split('/').pop();
            device.room = roomName;
            if (eval("typeof(RoomService." + device.type + "_in_edit)") === "function") {
              device = eval("RoomService." + device.type + "_in_edit(device)");
            }
            data.push(device);
          }
        }
        success(data, room);
      }, fail);
    }
  };

  RoomService.getRoomsDevices = function (state, success, fail) {
    if (typeof(state) === "undefined") {
      state = success;
      success = fail;
    }

    RoomService.getRooms(function (response, mac) {

      // Rooms obtained, let's iterate
      for (var key in response.data) {


        RoomService.get(false, mac + "/" + response.data[key], function (devices, room) {
          var devicesArray = [];

          // Devices obtained
          for (var key2 in devices) {
            devices[key2].room = room;
            if (typeof state === "undefined" || typeof(state) === "function" || (typeof state != "undefined" && devices[key2].state == state)) {
              devicesArray.push(devices[key2]);
            }
          }

          if (typeof(state) === "function") {
            state(devicesArray)
          } else
            success(devicesArray);
        });

      }
    });
  };

  RoomService.getStatusDevices = function (success, fail) {

    ApiService.get("devices", success, fail);

  };

  RoomService.getMac = function (no, success, fail) {

    RoomService.getStatusDevices(function (r) {
      if (Object.keys(r.data).length > 0) {
        success(r.data[no].MAC);
      }
    });
  };

  RoomService.change = function (address, data, success, fail) {
    RoomService.getMac(RoomService.actual, function (mac) {
      ApiService.get("rooms/" + mac, function (r) {
        ApiService.put("devices/" + mac + "/" + address + "/action", data, success, fail);
      }, fail);
    });
  };

  RoomService.alter = function (array, data) {
    var search = "address";

    for (var i = 0; i < array.length; i++) {
      if (typeof(array[i][search]) === "undefined") {
        for (var j = 0; j < RoomService.devices.length; j++) {
          if (eval("RoomService." + RoomService.devices[j] + "_update(array, data)")) { // successfully updated means true
            return array;
          }
        }
      } else if (array[i][search] == data[search]) {
        angular.extend(array[i], data);
        return array;
      }
    }

    return array;
  };

  // Device methods

  RoomService.rgba_in_edit = function (data) {
    data.colors = {
      r: data.states.R * 2.55,
      g: data.states.G * 2.55,
      b: data.states.B * 2.55
    };

    data.states.A_old = (data.states.A) ? data.states.A : 100;

    return data;
  };

  RoomService.light_switch_update = function (array, data) {
    return false; // solved with normal method of search, therefore nothing can happen
  };

  RoomService.light_dimmer_update = function (array, data) {
    return false; // solved with normal method of search, therefore nothing can happen
  };

  // Updates blinds states (up and down)
  RoomService.blinds_update = function (array, data) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].type != "blinds")
        continue;

      var adr_name, found = false;
      for (adr_name in array[i].addresses) {
        if (array[i]["addresses"][adr_name] == data.address) {
          found = true;
          break;
        }
      }

      if (found) {
        array[i]["states"][adr_name] = data.state;
        return true;
      }
    }

    return false;
  };

  // Updates rgba states
  RoomService.rgba_update = function (array, data) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].type != "rgba")
        continue;

      var adr_name, found = false;
      for (adr_name in array[i].addresses) {
        if (array[i]["addresses"][adr_name] == data.address) {
          found = true;
          break;
        }
      }

      if (found) {
        array[i]["states"][adr_name] = data.state;
        if (adr_name != "A")
          array[i]["colors"][adr_name.toLowerCase()] = data.state * 2.55;
        return true;
      }
    }

    return false;
  };

  return RoomService;
}]);

(function () {
  'use strict';

  google.charts.load("current", {packages: ["corechart"]});

  var app = angular.module('application', [
      'ui.router',
      'ngAnimate',
      'pascalprecht.translate',
      'ngWebSocket',

      //foundation
      'foundation',
      'foundation.dynamicRouting',
      'foundation.dynamicRouting.animations',

      //custom
      'Authentication',
      'Rooms'
    ])
    .factory('loginChecker', ["$q", "$injector", function ($q, $injector) {
      var loginChecker = {};

      loginChecker.responseError = function (response) {
        if (response.status == "401") {
          var AuthService = $injector.get('AuthService');
          AuthService.logOut(function () {
          });
        }
      };

      return loginChecker;
    }])
    .config(config)
    .run(run);

  config.$inject = ['$urlRouterProvider', '$locationProvider', '$translateProvider', "$httpProvider"];

  function config($urlProvider, $locationProvider, $translateProvider, $httpProvider) {
    $httpProvider.interceptors.push('loginChecker');

    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });

    $translateProvider.translations('en', {
      'LIVING_ROOM': 'Living Room',
      'BED_ROOM': 'Bed Room',
      'KITCHEN': 'Kitchen',
      'HALL': 'Hall',
      "up": "UP",
      "down": "DOWN",
      "device": "DEVICE"
    });

    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.preferredLanguage('en');
  }

  function run($rootScope, $location, $state, AuthService, RoomService) {
    FastClick.attach(document.body);

    $rootScope.loggedIn = AuthService.isLoggedIn();
    if ($rootScope.loggedIn)
      $rootScope.user = AuthService.getUser();

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams) {
      if (typeof toState.data.vars.requireLogin !== "undefined" && toState.data.vars.requireLogin && (typeof $rootScope.loggedIn === "undefined" || !$rootScope.loggedIn)) {
        event.preventDefault();
        $state.go("entry");
      }
    });

    $rootScope.logout = function () {
      AuthService.logOut(function () {
      });
    };

    $rootScope.testUrl = function ($testing) {
      return $state.current.name.indexOf($testing) > -1;
    };

    $rootScope.statusBarDevices = [];
    $rootScope.statusBarActive = RoomService.actual;
    $rootScope.changeStatusIndex = function (i) {
      $rootScope.statusBarActive = i;
      RoomService.actual = i;
      $state.reload();
    };
    RoomService.getStatusDevices(function (response) {
      $rootScope.statusBarDevices = response.data;
    });
  }

  /** CONTROLLERS **/
  /** CAN BE CHANGED! change it as you need, this is only for demonstration. **/

  app.controller("EntryController", function ($scope, AuthService, $state, $rootScope) {

    if ($rootScope.loggedIn)
      $state.go("devices/dashboard");

    $scope.formChooser = true;
    $scope.registerData = {
      "username": "",
      "password": "",
      "passwordCheck": "",
      "email": "",
      "agree": false
    };

    $scope.loginData = {
      "username": "",
      "password": ""
    };

    $scope.login = function () {
      AuthService.logIn($scope.loginData.username, $scope.loginData.password, function () {
        $rootScope.loggedIn = true;
        $rootScope.user = AuthService.getUser();
        $state.go("devices/dashboard");
      }, function () {
        $scope.unsuccessfulLogin = true;
      });
    }

  });

  app.controller("DevicesController", ["$scope", "$rootScope", "$state", "RoomService", function ($scope, $rootScope, $state, RoomService) {
    $scope.currentState = $state;

    $scope.dashboardDevices = [
      {
        name: "Light",
        room: "Kitchen",
        status: false,
        favourite: true
      },
      {
        name: "Blinds",
        room: "Living room",
        status: true,
        favourite: false
      }
    ];

    $scope.intercomDevices = [
      {
        name: "Thomas",
        device: "iPAD",
        phone: "CZ 777 027 229"
      },
      {
        name: "Jane",
        device: "intercom",
        phone: "-"
      }
    ];
    $scope.dashboardOrder = "name";
    $scope.intercomOrder = "name";
    $scope.intercomEdit = -1;
    $scope.rangeTest = 15;
    $scope.selectedCamera = -1;
    $scope.val = 2;

    $scope.selectCamera = function (i) {
      if ($scope.selectedCamera != i)
        $scope.selectedCamera = i;
    };

    $scope.energy = function () {
      google.charts.setOnLoadCallback(drawDonutChart);

      function drawDonutChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['1', 49],
          ['2', 14],
          ['3', 37],
        ]);

        var options = {
          pieHole: 0.4,
          colors: ["#8796A0", "#19B4FF", "#F1C925"],
          legend: "none",
          chartArea: {height: "90%"}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graph-donut'));
        chart.draw(data, options);
      }
    };

    $scope.selectItems = {
      0: "Day",
      1: "Yesterday",
      2: "7 Days",
      3: "30 Days",
      4: "This month",
      5: "Last month",
      6: "Period"
    };
    $scope.selectedItem = 1;

    // ukázka práce s kalendářem (nutné dělat přes scope aby se projevily změny)
    $scope.calendarActive = "2016-7-10,2016-7-12";
    $scope.calendarClick = function (d) {
      var cal = angular.element(document.querySelector("il-calendar"));
      var pos = $scope.calendarActive.split(",").indexOf(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());

      if (pos > -1) {
        var splitted = $scope.calendarActive.split(",");
        splitted.splice(pos, 1);
        if (!$rootScope.$$phase)
          $scope.$apply(function () {
            $scope.calendarActive = splitted.join(",");
          });
        return;
      }


      var t = $scope.calendarActive.split(",");
      if (t.length >= 2) {
        t.pop();
      }

      t.push(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());

      if (!$rootScope.$$phase)
        $scope.$apply(function () {
          $scope.calendarActive = t.join(",");
        });
    };

    $scope.calendarMonth = "2016-8";

    $scope.calendarMonthChange = function (m) {
      //console.log(m);
    };

    $scope.selectFunc = function (name, key) {
      //console.log("položka změněna");
    };

    $scope.cards = {
      light: true,
      lamp: true,
      hall: false
    };
    $scope.switchChange = function (name, value) {
      switch (name) {
        case "light":
          $scope.cards.light = value;
          break;
        case "lamp":
          $scope.cards.lamp = value;
          break;
        case "hall":
          $scope.cards.hall = value;
          break;
      }
    };

    $scope.hourData = {
      '2016-11-04-01': 5414,
      '2016-11-08-02': 2014,
      '2016-11-08-15': 3921
    };

    $scope.testVar = "as";

    $scope.lineData = [
      {0: 0, 0.25: 352, 1: 652, 2: 462, 3: 215, 4: 952, 5: 16, 6: 0, 7.16: 1000, 8: 365, 9: 412, 10: 548, 11: 258, 12: 258, 15: 651, 16: 25, 17: 69, 18: 78, 19: 458, 19.58: 920, 20: 352, 21: 245, 22: 852, 23: 756, 24: 352},
      {
        0: 0,
        0.58: 756,
        1.2: 279,
        2: 145,
        3: 495,
        4: 916,
        5: 735,
        6.7: 290,
        7: 358,
        8: 959,
        9.5: 408,
        10: 533,
        11.8: 374,
        13: 499,
        14: 614,
        15: 664,
        16: 952,
        17: 907,
        18: 990,
        19.33: 832,
        19.5: 22,
        20: 682,
        20.6: 963,
        21: 725,
        22: 430,
        22.42: 581,
        23: 317,
        23.5: 505,
        24: 132
      },
      {
        0: 500,
        1: 500,
        2: 500,
        3: 500,
        4: 500,
        5: 500,
        6: 500,
        7: 500,
        8: 500,
        9: 500,
        10: 500,
        11: 500,
        12: 500,
        13: 500,
        14: 500,
        15: 500,
        16: 500,
        17: 500,
        18: 500,
        19: 500,
        20: 500,
        21: 500,
        22: 500,
        23: 500,
        24: 500,
      }
    ];

    $scope.lineDataChange = function () {
      $scope.lineData = [
        {0: 0, 0.75: 252, 1: 552, 2: 462, 3: 25, 4: 902, 5: 162, 6: 0, 7.16: 0, 8: 35, 9.25: 812, 10: 548, 11: 258, 12: 258, 15: 651, 16: 25, 17: 69, 18: 78, 19: 458, 19.58: 920, 20: 352, 21: 245, 22: 852, 23: 756, 24: 352},
        {
          0: 500,
          1: 500,
          2: 500,
          3: 500,
          4: 500,
          5: 500,
          6: 500,
          7: 500,
          8: 500,
          9: 500,
          10: 500,
          11: 500,
          12: 500,
          13: 500,
          14: 500,
          15: 500,
          16: 500,
          17: 500,
          18: 500,
          19: 500,
          20: 500,
          21: 500,
          22: 500,
          23: 500,
          24: 500,
        }
      ];
    }

    $scope.lineLabels = ["yesterday", "today", "average"];

    $scope.counterChangeFn = function (name, val) {
      //console.log(name + " | " + val);
    }

    $scope.colorpickerChangeFn = function (name, val) {
      //console.log(name + " | " + val);
    }

  }]);

  app.controller("DashboardController", ["$scope", "RoomService", "$websocket", function ($scope, RoomService, $websocket) {
    $scope.deviceList = [];
    $scope.dashboardOrder = "order";
    $scope.greaterThan = function (prop, val) {
      return function (item) {
        return item[prop] > val;
      }
    };

    RoomService.getRoomsDevices(function (devices) {
      $scope.deviceList = $scope.deviceList.concat(devices);
    });

    var dataStream = $websocket("wss://www.elkoep.cloud/wssapp/v1");
    dataStream.onOpen(function () {
      console.log("Connection established.");
    });
    dataStream.onClose(function () {
      console.log("Connection closed.");
    });
    dataStream.onMessage(function (r) {
      RoomService.alter($scope.deviceList, JSON.parse(r.data));
    })
  }]);

  app.controller("RoomsController", ["$scope", "RoomService", "$stateParams", "$rootScope", "$websocket", "$state", function ($scope, RoomService, $stateParams, $rootScope, $websocket, $state) {
    $scope.detail = !(typeof($stateParams.roomName) === "undefined" || $stateParams.roomName === "");
    $scope.deviceList = [];

    // switch reaction
    $scope.switched = function (name, value, item, type) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      var address, state;

      switch (type) {
        case "rgba" :
          address = "rgba";
          state = {
            state: [
              device[0].addresses.R, device[0].colors.r / 2.55,
              device[0].addresses.G, device[0].colors.g / 2.55,
              device[0].addresses.B, device[0].colors.b / 2.55,
              device[0].addresses.A, value ? device[0].states.A_old : 0
            ]
          };
          break;

        default:
          address = device[0].address;
          state = {"state": value ? 1 : 0};
      }

      RoomService.change(address, state, function (r) {
        //console.log("as");
      });
    };

    // blinds change
    $scope.changeBlinds = function (device, dir) {
      var address = false, state = false;

      switch (dir) {
        case "up":
          if (device.states.down == 1) { // down is one => send zero to down
            state = {state: 0};
            address = device.addresses.down;
          } else if (device.states.up == 0) { // down is zero and up as well => send one to up
            state = {state: 1};
            address = device.addresses.up;
          }
          break;
        case "down":
          if (device.states.up == 1) { // up is one => change state of up to zero on address of up
            state = {state: 0};
            address = device.addresses.up;
          } else if (device.states.down == 0) { // up is zero and down as well => change state of down to 1 on address of down
            address = device.addresses.down;
            state = {state: 1};
          }
          break;
      }

      if (address !== false && state !== false)
        RoomService.change(address, state, function (r) {
        });
    };

    // range change (dimmer)
    $scope.ranged = function (name, value) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      RoomService.change(device[0].address, {"state": value}, function (r) {

      });
    };

    $scope.changeColorPicker = function (name, color) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      device = device[0];

      var states = {
        state: [
          device.addresses.R, color.r / 2.55,
          device.addresses.G, color.g / 2.55,
          device.addresses.B, color.b / 2.55,
          device.addresses.A, device.states.A
        ]
      };

      RoomService.change("rgba", states, function (r) {
      });
    };

    // layout switch
    if ($scope.detail) { // detail of rooms with cards
      $scope.room = $stateParams.roomName;
      $scope.i = 0;

      RoomService.get(true, $stateParams.roomName, function (data) {
        $scope.deviceList = data;
      });

      // websockets
      var dataStream = $websocket("wss://www.elkoep.cloud/wssapp/v1");
      dataStream.onOpen(function () {
        console.log("Connection established.");
      });
      dataStream.onClose(function () {
        console.log("Connection closed.");
      });
      dataStream.onMessage(function (r) {
        $scope.deviceList = RoomService.alter($scope.deviceList, JSON.parse(r.data), "addresses");
      })
    } else { // list of rooms
      RoomService.getRooms(function (r) {
        var keys = Object.keys(r.data);
        if (keys.length == 1) {
          $state.go("devices/rooms", {roomName: r.data[keys[0]]});
        }
        $scope.rooms = r.data;

      }), function (r) {
        //console.log(r);
      };
    }
  }]);

  /** DIRECTIVES **/
  /** DO NOT CHANGE! unless it's really necessary. **/

  app.directive('passwordValidation', ['$parse', function ($parse) {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {
        scope.$watch(function () { // matching validation
          return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) || $parse(attrs.passwordValidation)(scope) === ctrl.$modelValue;
        }, function (currentValue) {
          ctrl.$setValidity('match', currentValue);
        });

        ctrl.$parsers.unshift(function (viewValue) { // strength (content) validation
          var pwdValidLength, pwdHasULetter, pwdHasLLetter, pwdHasNumber, pwdHasSpecial;

          pwdValidLength = (viewValue && viewValue.length >= 8) ? true : false;
          pwdHasULetter = (viewValue && /[A-Z]/.test(viewValue)) ? true : false;
          pwdHasLLetter = (viewValue && /[a-z]/.test(viewValue)) ? true : false;
          pwdHasNumber = (viewValue && /\d/.test(viewValue)) ? true : false;
          pwdHasSpecial = (viewValue && /[^a-zA-Z0-9]/.test(viewValue)) ? true : false;

          ctrl.$setValidity('pwd', pwdValidLength && pwdHasULetter && pwdHasLLetter && pwdHasNumber && pwdHasSpecial);
          return viewValue;
        });
      },
    };
  }]);


  app.directive("ilHeader", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/site/header.html"
    }
  });

  app.directive("ilFooter", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/site/footer.html"
    }
  });

  app.directive("ilStatusBar", function (RoomService) {
    return {
      restrict: "E",
      templateUrl: "./templates/devices/status_bar.html"
    }
  });

  app.directive("ilDevicesNavigation", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/devices/navigation.html"
    }
  });

  app.directive("ilSwitch", function () {
    return {
      restrict: "E",
      scope: {
        name: "@",
        checked: "=",
        changeFn: "=",
        ngModel: "=",
        item: "=",
        type: "@",
        disabled: "="
      },
      link: function (scope, element, attrs) {

        // tried change state
        element.find("label").on("click", function (e) {
          if (typeof(scope.disabled) !== "undefined" && scope.disabled) {
            return;
          }

          if (typeof(scope.ngModel !== "undefined")) {
            scope.ngModel = !scope.switchbox;
          }

          if (typeof(scope.changeFn) !== "undefined") {
            scope.changeFn(scope.name, !scope.switchbox, scope.item, scope.type);
          }
        });

        // if model is not undefined, set default value
        if (typeof scope.ngModel != "undefined") {
          scope.switchbox = scope.ngModel >= 1 || scope.ngModel;
        }
        else if (typeof scope.checked != "undefined")
          scope.switchbox = scope.checked;

        if (typeof(scope.ngModel) !== "undefined") {
          scope.$watch("ngModel", function (nValue) {
            scope.switchbox = nValue >= 1 || nValue;
          })
        }
      }

      ,
      template: '<input type="checkbox" name="{{ name }}" id="switch-{{ name }}" ng-model="switchbox" disabled="disabled"><label for="switch-{{ name }}"></label>'
    }
  });

  app.directive("ilRange", function () {
    return {
      restrict: "E",
      scope: {
        name: "@",
        value: "=",
        changeFn: "=",
        ngModel: "=",
      },
      link: function (scope, element, attrs) {

        element.find("input").on("change", function (e) {
          if (typeof(scope.ngModel !== "undefined")) {
            scope.ngModel = scope.range;
          }

          if (typeof(scope.changeFn) !== "undefined") {
            scope.changeFn(scope.name, scope.range);
          }
        });

        if (typeof scope.ngModel != "undefined") {
          scope.range = !isNaN(scope.ngModel) && Number.isInteger(scope.ngModel) ? scope.ngModel * 100 : scope.ngModel;
        }
        else if (typeof scope.checked != "undefined")
          scope.range = scope.value;

        if (typeof(scope.ngModel) !== "undefined") {
          scope.$watch("ngModel", function (nValue) {
            scope.range = !isNaN(nValue) && Number.isInteger(nValue) ? nValue * 100 : nValue;
          })
        }
      },
      template: '<input type="range" min=0 max=100 ng-model="ngModel" name="{{name}}" id="range-{{name}}" value="0">'
    }
  });

  app.directive("ilCard", function () {
    return {
      restrict: "CA",
      link: function (scope, element, attrs) {
        var opener = angular.element(element[0].querySelector("a.open"));


        opener.on("click", function (e) {
          e.preventDefault();

          if (element.hasClass("opened")) {
            element.removeClass("opened");
            element.addClass("closed");
          } else {
            element.removeClass("closed");
            element.addClass("opened");
          }
        });
      }
    }
  });

  app.directive("ilCounter", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        scope.ilnumber = (typeof attrs.value == "undefined") ? 0 : parseInt(attrs.value);
        var input = angular.element(element[0].querySelector("input"));
        var plus = angular.element(element[0].querySelector(".plus"));
        var minus = angular.element(element[0].querySelector(".minus"));
        var changeElement = null;
        input.attr("name", attrs.name);

        function changeValueIn(val) {
          changeElement.text(val);
        }

        if (typeof attrs.changeIn != "undefined") {
          changeElement = angular.element(document.querySelector(attrs.changeIn));
        }

        scope.$watch("ilnumber", function (nValue) {
          if (changeElement != null) {
            element.attr("value", nValue);
            changeValueIn(nValue);
          }

          if (typeof scope.changeFn != "undefined") {
            scope.changeFn(attrs.name, nValue);
          }
        });

        plus.on("click", function (e) {
          if (!scope.$$phase)
            scope.$apply(function () {
              scope.ilnumber++;
            });
        });

        minus.on("click", function (e) {
          if (!scope.$$phase)
            scope.$apply(function () {
              scope.ilnumber--;
            });
        });
      },
      scope: {
        changeFn: "=",
      },
      template: '<input type="number" ng-model="ilnumber" name=""><span class="minus">-</span><span class="plus">+</span>'
    };
  });

  app.directive("ilColorpicker", function () {
    return {
      restrict: "E",
      scope: {
        changeFn: "=",
        value: "="
      },
      link: function (scope, element, attrs) {
        var input = angular.element(element[0].querySelector("input"));
        var opener = angular.element(element[0].querySelector("a.opener"));
        var color_indicator = angular.element(element[0].querySelector(".color-indicator"));
        var picker = angular.element(element[0].querySelector(".picker-wrapper"));
        var slider = angular.element(element[0].querySelector(".color-slider"));


        ColorPicker.fixIndicators(element[0].querySelector(".slider-indicator"), element[0].querySelector('.picker-indicator'));
        var col = ColorPicker(element[0].querySelector(".color-slider"), element[0].querySelector(".color-picker"), function (hex, hsv, rgb, cursorPicker, cursorSlider) {
          input.attr("value", hex);
          color_indicator[0].style.backgroundColor = hex;
          scope.hex = hex;
          scope.rgb = rgb;

          ColorPicker.positionIndicators(
            element[0].querySelector(".slider-indicator"),
            element[0].querySelector('.picker-indicator'),
            cursorSlider, cursorPicker
          );
        });

        function changeIn() {
          scope.value = scope.rgb;
          if (typeof attrs.changeIn != "undefined") {
            angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
          }

          if (typeof scope.changeFn != "undefined") {
            scope.changeFn(attrs.name, scope.rgb);
          }
        }

        if (typeof scope.value != "undefined") {
          scope.actualValue = {};
          col.setRgb(scope.value);
          angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
          angular.copy(scope.value, scope.actualValue);
          color_indicator[0].style.backgroundColor = "rgb(" + scope.value.r + ", " + scope.value.g + ", " + scope.value.b + ")";

          setInterval(function () {
            if (!angular.equals(scope.value, scope.actualValue)) {
              col.setRgb(scope.value);
              angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
              angular.copy(scope.value, scope.actualValue);
            }
          }, 1000);
        } else {
          input.attr("value", "#FFFFFF");
          col.setHex("#FFFFFF");
        }

        if (typeof attrs.name != "undefined") {
          input.attr("name", attrs.name);
        }

        picker.on("click", function (e) {
          changeIn();
        });

        slider.on("click", function (e) {
          changeIn();
        });

        opener.on("click", function (event) {
          event.preventDefault();

          if (picker.hasClass("opened")) {
            picker.removeClass("opened");
            picker.addClass("closed");
          } else {
            picker.removeClass("closed");
            picker.addClass("opened");
          }
        });
      },
      template: '<input type="color" name=""><div class="grid-block"><div class="grid-content"><div class="slider-wrapper"><span class="slider-indicator"></span><div class="color-slider"></div></div></div><div class="grid-content shrink"><a href="#" class="color-indicator opener"></a><div class="picker-wrapper closed"><span class="picker-indicator"></span><div class="color-picker"></div></div></div></div>'
    }
  });

  app.directive("ilScale", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        var min = parseInt(attrs.min);
        var max = parseInt(attrs.max);
        var step = parseInt(attrs.step);
        var active = typeof attrs.active == "undefined" ? null : parseInt(attrs.active);
        var postfix = typeof attrs.postfix == "undefined" ? "" : attrs.postfix;
        var prefix = typeof attrs.prefix == "undefined" ? "" : attrs.prefix;

        var colorStart = typeof attrs.colorStart == "undefined" ? null : attrs.colorStart;
        var colorEnd = typeof attrs.colorEnd == "undefined" ? null : attrs.colorEnd;
        var color = null;
        var gradient = null;

        if (colorStart == colorEnd && colorStart != null) {
          color = colorStart;
        } else if (colorStart != null || colorEnd != null) {
          if (colorStart == null)
            colorStart = "#8796A0";
          else if (colorEnd == null)
            colorEnd = "#8796A0";

          gradient = new Rainbow;
          gradient.setNumberRange(min, max);
          gradient.setSpectrum(colorStart, colorEnd);
        }

        for (var i = min; i <= max; i += step) {
          var item = angular.element('<span class="item ' + ((i == active) ? "active" : "") + '" value="' + i + '">' + prefix + i + postfix + '</span>');
          if (color != null)
            item[0].style.color = color;

          if (gradient != null) {
            item[0].style.color = "#" + gradient.colourAt(i);
          }

          element.append(item);
        }
      },
      template: ""
    }
  });

  app.directive("color", function () {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element[0].style.color = attrs.color;
      }
    }
  });

  app.directive("ilMeter", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        element[0].style.visibility = "hidden";
        var min = parseInt(scope.min);
        var max = parseInt(scope.max);
        var points = 8; // + 0 = 9 points
        var step = (Math.abs(max - min) / points);


        for (var i = min, j = 0; i <= max; i += step, j++) {
          var unit = angular.element('<span class="unit u-' + j + '">' + Math.round(i * 100) / 100 + '</span>');
          element.prepend(unit);
        }

        element[0].style.visibility = "visible";

      },
      controller: function ($scope, $element, $attrs) {
        function set_position(val) {
          $element[0].querySelector(".pointer").style.top = (1 - (val - $scope.min) / Math.abs($scope.max - $scope.min)) * 100 + "%";
        }

        set_position($scope.value);
      },
      scope: {
        value: "@",
        min: "@",
        max: "@"
      },
      template: '<span class="pointer"></span><span class="meter"></span>'
    }
  });

  app.directive("responsiveScroller", function () {
    return {
      restrict: "AC",
      link: function ($scope, $element, $attrs) {

        var padding = "16";

        function changeSize() {
          $element[0].style.maxWidth = (document.body.clientWidth - padding * 2) + "px";
        }

        if (typeof $attrs.minWidth != "undefined") {
          $element.children()[0].style.minWidth = $attrs.minWidth + "px";
          $element.children()[0].style.display = "block";
        }

        changeSize();
        angular.element(window).on("resize", changeSize);
      }
    }
  });

  /**
   * After day selection calls function given in attribute click-fn
   * example: click-fn="calendarClick" calls function calendarClick
   */
  app.directive("ilCalendar", function () {
    return {
      restrict: "E",
      scope: {
        clickFn: "=",
        monthChangeFn: "="
      },
      link: function ($scope, $element, $attrs) {
        var weekDays = ["S", "M", "T", "W", "T", "F", "S"];
        var activeClasses = ["bg-blue", "bg-red"];
        $scope.clicked = -1;
        var gMonth = null;
        var monthNames = ["January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];

        function toDate(str) {
          if (typeof str != "string")
            return str;

          var parts = str.replace(" ", "").split("-").map(function (i) {
            return parseInt(i)
          });

          var date = new Date();
          for (var i = 0; i < parts.length; i++) {
            switch (i) {
              case 0:
                date.setFullYear(parts[i]);
                break;
              case 1:
                date.setMonth(parts[i] - 1);
                break;
              case 2:
                date.setDate(parts[i]);
                break;
              case 3:
                date.setHours(parts[i]);
                break;
              case 4:
                date.setMinutes(parts[i]);
                break;
            }
          }

          return date;
        }

        function render(month) {
          $element.html("");
          $element.attr("month", month.getFullYear() + "-" + (month.getMonth() + 1));
          var monthDuplicate = new Date(month.getFullYear(), month.getMonth(), month.getDate());
          gMonth = new Date(month.getFullYear(), month.getMonth(), month.getDate());
          monthDuplicate.setDate(0);
          var days = monthDuplicate.getDate();
          month.setDate(1);

          var months = angular.element("<table class='month-list'><tr><td class='previous'></td><td class='current'></td><td class='next'></td></tr></table>");
          var table = angular.element("<table></table>");
          var thead = angular.element("<thead><tr></tr></thead>");
          var tbody = angular.element("<tbody><tr></tr></tbody>");

          angular.element(months[0].querySelector(".current")).text(monthNames[month.getMonth()]);
          monthDuplicate = new Date(month.getFullYear(), month.getMonth() - 1, month.getDate());
          var prev = angular.element(months[0].querySelector(".previous")).text(monthNames[monthDuplicate.getMonth()]);
          monthDuplicate = new Date(month.getFullYear(), month.getMonth() + 1, month.getDate());
          var next = angular.element(months[0].querySelector(".next")).text(monthNames[monthDuplicate.getMonth()]);


          prev.on("click", function (event) {
            event.preventDefault();
            month = toDate($attrs.month);
            month.setMonth(month.getMonth() - 1);
            $attrs.month = month.getFullYear() + "-" + (month.getMonth() + 1);
            if (typeof $scope.monthChangeFn != "undefined") {
              $scope.monthChangeFn(month);
            }
            startRendering($attrs.month);
          });

          next.on("click", function (event) {
            event.preventDefault();
            month = toDate($attrs.month);
            month.setMonth(month.getMonth() + 1);
            $attrs.month = month.getFullYear() + "-" + (month.getMonth() + 1);
            if (typeof $scope.monthChangeFn != "undefined") {
              $scope.monthChangeFn(month);
            }
            startRendering($attrs.month);
          });

          table.append(thead);
          table.append(tbody);

          thead = thead.find("tr");
          tbody = tbody.find("tr");
          for (var i = 1; i <= days; i++) {
            month.setDate(i);
            var th = angular.element("<th>" + weekDays[month.getDay()] + "</th>");
            var td = angular.element("<td class='day-" + i + "'><span class='day' value='" + i + "'>" + i + "</span></td>");

            var span = td.find("span");
            span.on("click", function (event) {
              var clickedItem = angular.element(this);
              $scope.clicked = parseInt(clickedItem.attr("value"));
              $element.attr("last-clicked", $scope.clicked);

              if (typeof $scope.clickFn != "undefined")
                $scope.clickFn(new Date(month.getFullYear(), month.getMonth(), clickedItem.attr("value")));
            });

            if (month.getDay() == 0 || month.getDay() == 6)
              td.addClass("color-blue");

            thead.append(th);
            tbody.append(td);
          }

          $element.append(months);
          $element.append(table);

          if (typeof $attrs.active != "undefined")
            activeReact($attrs.active);
        }

        function startRendering(dateStr) {
          gMonth = toDate(dateStr);
          render(toDate(dateStr));
        }

        function activeReact(activeStr) {

          var actives = activeStr.replace(" ", "").split(",").map(function (i) {
            return toDate(i)
          });

          setActive(gMonth, actives);
        }

        function setActive(month, actives) {
          for (var i = 0; i < activeClasses.length; i++) {
            angular.element($element[0].querySelectorAll("td")).removeClass(activeClasses[i]);
          }

          for (var i = 0; i < actives.length; i++) {
            if (month.getFullYear() == actives[i].getFullYear() && month.getMonth() == actives[i].getMonth()) {
              angular.element($element[0].querySelector("td.day-" + actives[i].getDate())).addClass(activeClasses[i % activeClasses.length]);
            }
          }
        }

        $attrs.$observe("month", function (dateStr) {
          startRendering(dateStr);
        });

        $attrs.$observe("active", function (activeStr) {
          activeReact(activeStr);
        });
      }
    }
  });

  /**
   * After select calls function given in attribute select-fn
   * example: select-fn="selectFunc" calls function selectFunc with attribute selected value
   */
  app.directive("ilSelect", function () {
    return {
      restrict: "E",
      link: function ($scope, $element, $attrs) {

        var select = angular.element($element[0].querySelector("select"));
        var aItem = angular.element($element[0].querySelector(".selected-item"));
        var items = angular.element($element[0].querySelector(".items"));

        $scope.selected = null;

        if (typeof $attrs.selectedItem != "undefined")
          $scope.selected = Object.keys($scope.items)[0];

        select.attr("name", $scope.name);

        function render() {
          for (var key in Object.keys($scope.items)) {
            select.append('<option value="' + key + '">' + $scope.items[key] + '</option>');
            var item = angular.element('<div class="item" key="' + key + '">' + $scope.items[key] + '</div>');

            item.on("click", function (e) {
              $scope.selected = angular.element(this).attr("key");
              selected();
            });

            items.append(item);
          }
        }

        function selected() {
          if ($scope.selected != null) {
            $element.attr("selected-item", $scope.selected);
            aItem.text($scope.items[$scope.selected]);

            select.find("option").removeAttr("selected");
            angular.element(select[0].querySelector("option[value='" + $scope.selected + "']")).attr("selected", "selected");

            $element.removeClass("opened");

            if (typeof $scope.selectFn != "undefined") {
              $scope.selectFn($scope.name, $scope.selected);
            }

            if (typeof $scope.bind != "undefined") {
              $scope.bind = $scope.selected;
            }
          }
        }

        aItem.on("click", function (event) {
          $element.toggleClass("opened");
        });

        $attrs.$observe("selectedItem", function (newValue) {
          $scope.selected = newValue;
          selected();
        });

        render();
      },
      scope: {
        items: "=",
        selectFn: "=",
        name: "@",
      },
      template: "<select name=''></select><div class='selected-item'></div><div class='items'></div>"
    }
  });

  app.directive("ilHourChart", function () {
    return {
      restrict: "E",
      scope: {
        data: "=",
        from: "@",
        to: "@",
        sum: "="
      },
      link: function (scope, element, attrs) {

        scope.fromColor = "#1ab2ff";
        scope.toColor = "#ed2e26";
        scope.gradient = new Rainbow;

        var rendering = false;

        function humanize(num) {
          var ret = "";

          var i = 0;
          for (; num >= 1000; i++) {
            num = Math.round(num / 100) / 10;
          }

          ret = num;
          switch (i) {
            case 1:
              ret += "k";
              break;
            case 2:
              ret += "M";
              break;
            case 3:
              ret += "G";
              break;
            case 4:
              ret += "T";
              break;
          }

          return ret;
        }

        function getMaxValue() {
          var max = 0;

          for (var item in scope.data) {
            if (scope.data[item] > max)
              max = scope.data[item];
          }

          return max;
        }

        function toDate(str) {
          if (typeof str != "string")
            return str;

          var parts = str.replace(" ", "").split("-").map(function (i) {
            return parseInt(i)
          });

          var date = new Date();
          for (var i = 0; i < parts.length; i++) {
            switch (i) {
              case 0:
                date.setFullYear(parts[i]);
                break;
              case 1:
                date.setMonth(parts[i] - 1);
                break;
              case 2:
                date.setDate(parts[i]);
                break;
              case 3:
                date.setHours(parts[i]);
                break;
              case 4:
                date.setMinutes(parts[i]);
                break;
            }
          }

          return date;
        }

        function normalizeDate() {
          for (var item in scope.data) {
            var date = toDate(item);
            var value = scope.data[item];
            scope.normalized[date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getHours()] = value;
          }
        }

        function render() {
          rendering = true;
          scope.normalized = {};
          normalizeDate();

          scope.minValue = 0;
          scope.maxValue = getMaxValue();

          if (scope.minValue == scope.maxValue)
            scope.maxValue++;

          scope.gradient.setNumberRange(scope.minValue, scope.maxValue);
          scope.gradient.setSpectrum(scope.fromColor, scope.toColor);

          scope.fromDate = toDate(attrs.from);
          scope.toDate = toDate(attrs.to);

          element.html("");

          var wrapper = angular.element('<div class="wrapper"></div>');
          var table = angular.element('<table></table>');
          var thead = angular.element('<thead><tr></tr></thead>');
          var tbody = angular.element('<tbody></tbody>');

          table.append(thead);
          table.append(tbody);

          // table heading (hours + one empty for first column + one empty for sum column)
          thead = thead.find("tr");
          thead.append("<th></th>");
          for (var i = 1; i < 25; i++) {
            thead.append("<th>" + i + "</th>");
          }

          if (scope.sum)
            thead.append("<th></th>");

          // data print
          var days = Math.ceil(Math.abs(scope.toDate.getTime() - scope.fromDate.getTime()) / (1000 * 3600 * 24)) + 1;
          var from = scope.fromDate;

          for (var i = 0; i < days; i++, from.add) {
            var row = angular.element("<tr></tr>");
            var day = from.getFullYear() + "-" + (from.getMonth() + 1) + "-" + from.getDate();

            row.append("<td>" + (from.getMonth() + 1) + "/" + ((from.getDate() > 9) ? from.getDate() : ("0" + from.getDate())) + "</td>");

            var sum = 0;
            for (var j = 1; j <= 24; j++) {
              var td = angular.element('<td><span class="data"></span></td>');
              var data = td.find("span");
              data.text(scope.minValue);

              if (typeof scope.normalized[day + "-" + j] != "undefined") {
                td.append('<span class="bubble">' + scope.normalized[day + "-" + j] + '</span>');
                data.text(humanize(scope.normalized[day + "-" + j]));
                data[0].style.backgroundColor = "#" + scope.gradient.colourAt(scope.normalized[day + "-" + j]);
                sum += scope.normalized[day + "-" + j];
              } else {
                td.append('<span class="bubble">' + scope.minValue + '</span>');
                data[0].style.backgroundColor = "#" + scope.gradient.colourAt(scope.minValue);
              }
              row.append(td);
            }

            if (scope.sum)
              row.append('<td><span class="sum">' + humanize(sum) + '</span><span class="bubble">' + sum + '</span></td>');

            tbody.append(row);
            from.setDate(from.getDate() + 1);
          }

          var gradientBar = angular.element('<div class="gradient-bar"></div>');
          for (var i = scope.minValue; i < scope.maxValue; i += ((scope.maxValue - scope.minValue) / 9)) {
            var bar = angular.element('<div class="bar"></div>');
            bar[0].style.backgroundColor = "#" + scope.gradient.colourAt(i);
            gradientBar.append(bar);
          }
          gradientBar.append('<span class="min">' + humanize(scope.minValue) + '</span>');
          gradientBar.append('<span class="max">' + humanize(scope.maxValue) + '</span>');

          wrapper.append(table);
          wrapper.append(gradientBar);
          element.append(wrapper);
          rendering = false;
        }

        scope.$watch("data", function () {
          if (!rendering)
            render();
        });

        scope.$watch("from", function () {
          if (!rendering)
            render();
        });

        scope.$watch("to", function () {
          if (!rendering)
            render();
        });
      }
    }
  });

  app.directive("ilToggle", function () {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element.on("click", function (event) {
          event.preventDefault();
          angular.element(document.querySelectorAll(attrs.ilToggle)).toggleClass(attrs.ilToggleClass);
        });
      }
    }
  });

  app.directive("ilLineChart", function () {
    return {
      restrict: "E",
      scope: {
        data: "=",
        labels: "=",
        height: "@",
        width: "@",
        unit: "@",
        min: "@",
        max: "@"
      },
      link: function (scope, element, attrs) {
        var canvas = angular.element(element[0].querySelector("canvas"));
        var ctx = canvas[0].getContext("2d");
        var colors = ["#19b4ff", "#ff7a5c", "#f1c925"];
        var fontSize = 15;

        function setCanvas() {
          canvas.attr("height", scope.height);
          canvas.attr("width", scope.width);
        }

        function drawMatrix(padding) {

          // Base lines (left and bottom)
          ctx.strokeStyle = "#87959E";
          ctx.lineWidth = 3;
          ctx.beginPath();
          ctx.moveTo(padding.left, padding.top);
          ctx.lineTo(padding.left, scope.height - padding.bottom);
          ctx.lineTo(scope.width - padding.right, scope.height - padding.bottom);
          ctx.stroke();
          ctx.closePath();

          // Backgrounds
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          ctx.fillStyle = "#EDEEF0";
          for (var i = 0; i < 24; i++) {
            if (i % 2 != 0) {
              ctx.fillRect(padding.left + 3 + i * part, padding.top, part, scope.height - padding.top - padding.bottom - 3);
            }
          }

          // Dividing lines
          part = (scope.height - padding.top - padding.bottom - 3) / 10;
          ctx.setLineDash([4, 3]);
          ctx.strokeStyle = "#d6dbde";
          ctx.lineWidth = 1;
          ctx.beginPath();
          for (var i = 1; i < 10; i++) {
            ctx.moveTo(padding.left + 3, padding.top + i * part);
            ctx.lineTo(scope.width - padding.right, padding.top + i * part);
          }
          ctx.stroke();
          ctx.closePath();
          ctx.setLineDash([0, 0]);
        }

        function writeValues(textPadding, padding) {

          // Unit
          ctx.font = "15px Gotham";
          ctx.fillStyle = "#87959e";
          ctx.textAlign = "end";
          ctx.fillText(scope.unit, padding.left - textPadding, padding.top - textPadding);

          // Days
          ctx.textAlign = "center";
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          for (var i = 0; i < 24; i++) {
            ctx.fillText(i + 1, padding.left + i * part + part / 2, padding.top - textPadding);
          }

          // Values
          ctx.textAlign = "end";
          ctx.textBaseline = "middle";
          part = (scope.height - padding.top - padding.bottom - 3) / 10;
          var unit = (scope.max - scope.min) / 10;
          for (var i = 0; i < 10; i++) {
            ctx.fillText(unit * ( i + 1), padding.left - textPadding, scope.height - padding.bottom - 3 - part * i - part / 2);
          }
        }

        function renderLine(data, i, padding) {
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          var keys = [];
          var pixelRate = (scope.height - padding.top - padding.bottom - 3) / (scope.max - scope.min);
          for (var key in data) {
            keys.push(parseFloat(key));
          }
          keys.sort(function (a, b) {
            return a - b
          });

          ctx.strokeStyle = colors[i];
          ctx.lineWidth = 2;
          ctx.beginPath();
          ctx.moveTo(padding.left + 3 + keys[0] * part, scope.height - padding.bottom - 3 - (pixelRate) * data[keys[0]]);
          for (var i = 1; i < keys.length; i++) {
            ctx.lineTo(padding.left + 3 + keys[i] * part, scope.height - padding.bottom - 3 - (pixelRate) * data[keys[i]]);
          }
          ctx.stroke();
          ctx.closePath();
        }

        function renderLabels(textPadding, padding) {
          if (typeof scope.labels != "undefined") {
            var pWidth = 0;
            ctx.textAlign = "left";
            ctx.textBaseline = "hanging";
            for (var i = 0; i < scope.labels.length; i++) {
              ctx.fillStyle = colors[i % colors.length];
              ctx.fillRect(padding.left + pWidth - 3, scope.height - padding.top + textPadding.top, fontSize, fontSize);
              ctx.fillText(scope.labels[i], padding.left + pWidth - 3 + fontSize * 2, scope.height - padding.top + textPadding.top);
              pWidth += fontSize + textPadding.left + ctx.measureText(scope.labels[i]).width + fontSize * 2;
            }
          }
        }

        function renderLines(padding) {
          for (var i = 0; i < scope.data.length; i++) {
            renderLine(scope.data[i], i % colors.length, padding);
          }
        }

        function render() {
          var padding = {
            left: 60, top: 60, right: 0, bottom: 60
          };
          setCanvas();
          drawMatrix(padding);
          writeValues(5, padding);
          renderLines(padding);
          renderLabels({left: 10, top: 15}, padding);
        }

        render();
        scope.$watch("data", function () {
          render();
        });

        render();
        scope.$watch("labels", function () {
          render();
        })
      },
      template: "<canvas></canvas>"
    }
  });


  app.directive("ilDonutChart", function () {
    return {
      restriction: "E",
      scope: {
        data: "=",
        hoverFn: "=",
        height: "@",
        width: "@"
      },
      link: function (scope, element, attrs) {

      },
      template: "<canvas></canvas>"
    }
  });

})();

/**
 * ColorPicker - pure JavaScript color picker without using images, external CSS or 1px divs.
 * Copyright © 2011 David Durman, All rights reserved.
 */
(function (window, document, undefined) {

  var type = (window.SVGAngle || document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? "SVG" : "VML"),
    picker, slide, hueOffset = 0, svgNS = 'http://www.w3.org/2000/svg';

  // This HTML snippet is inserted into the innerHTML property of the passed color picker element
  // when the no-hassle call to ColorPicker() is used, i.e. ColorPicker(function(hex, hsv, rgb) { ... });

  var colorpickerHTMLSnippet = [

    '<div class="picker-wrapper">',
    '<div class="picker"></div>',
    '<div class="picker-indicator"></div>',
    '</div>',
    '<div class="slide-wrapper">',
    '<div class="slide"></div>',
    '<div class="slide-indicator"></div>',
    '</div>'

  ].join('');

  /**
   * Return mouse position relative to the element el.
   */
  function mousePosition(evt) {
    // IE:
    if (window.event && window.event.contentOverflow !== undefined) {
      return {x: window.event.offsetX, y: window.event.offsetY};
    }
    // Webkit:
    if (evt.offsetX !== undefined && evt.offsetY !== undefined) {
      return {x: evt.offsetX, y: evt.offsetY};
    }
    // Firefox:
    var wrapper = evt.target.parentNode.parentNode;
    return {x: evt.layerX - wrapper.offsetLeft, y: evt.layerY - wrapper.offsetTop};
  }

  /**
   * Create SVG element.
   */
  function $(el, attrs, children) {
    el = document.createElementNS(svgNS, el);
    for (var key in attrs)
      el.setAttribute(key, attrs[key]);
    if (Object.prototype.toString.call(children) != '[object Array]') children = [children];
    var i = 0, len = (children[0] && children.length) || 0;
    for (; i < len; i++)
      el.appendChild(children[i]);
    return el;
  }

  /**
   * Create slide and picker markup depending on the supported technology.
   */
  if (type == 'SVG') {

    slide = $('svg', {xmlns: 'http://www.w3.org/2000/svg', version: '1.1', width: '100%', height: '100%'},
      [
        $('defs', {},
          $('linearGradient', {id: 'gradient-hsv', x1: '100%', y1: '0%', x2: '0%', y2: '0%'},
            [
              $('stop', {offset: '0%', 'stop-color': '#FF0000', 'stop-opacity': '1'}),
              $('stop', {offset: '13%', 'stop-color': '#FF00FF', 'stop-opacity': '1'}),
              $('stop', {offset: '25%', 'stop-color': '#8000FF', 'stop-opacity': '1'}),
              $('stop', {offset: '38%', 'stop-color': '#0040FF', 'stop-opacity': '1'}),
              $('stop', {offset: '50%', 'stop-color': '#00FFFF', 'stop-opacity': '1'}),
              $('stop', {offset: '63%', 'stop-color': '#00FF40', 'stop-opacity': '1'}),
              $('stop', {offset: '75%', 'stop-color': '#0BED00', 'stop-opacity': '1'}),
              $('stop', {offset: '88%', 'stop-color': '#FFFF00', 'stop-opacity': '1'}),
              $('stop', {offset: '100%', 'stop-color': '#FF0000', 'stop-opacity': '1'})
            ]
          )
        ),
        $('rect', {x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-hsv)'})
      ]
    );

    picker = $('svg', {xmlns: 'http://www.w3.org/2000/svg', version: '1.1', width: '100%', height: '100%'},
      [
        $('defs', {},
          [
            $('linearGradient', {id: 'gradient-black', x1: '0%', y1: '100%', x2: '0%', y2: '0%'},
              [
                $('stop', {offset: '0%', 'stop-color': '#000000', 'stop-opacity': '1'}),
                $('stop', {offset: '100%', 'stop-color': '#CC9A81', 'stop-opacity': '0'})
              ]
            ),
            $('linearGradient', {id: 'gradient-white', x1: '0%', y1: '100%', x2: '100%', y2: '100%'},
              [
                $('stop', {offset: '0%', 'stop-color': '#FFFFFF', 'stop-opacity': '1'}),
                $('stop', {offset: '100%', 'stop-color': '#CC9A81', 'stop-opacity': '0'})
              ]
            )
          ]
        ),
        $('rect', {x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-white)'}),
        $('rect', {x: '0', y: '0', width: '100%', height: '100%', fill: 'url(#gradient-black)'})
      ]
    );

  } else if (type == 'VML') {
    slide = [
      '<DIV style="position: relative; width: 100%; height: 100%">',
      '<v:rect style="position: absolute; top: 0; left: 0; width: 100%; height: 100%" stroked="f" filled="t">',
      '<v:fill type="gradient" method="none" angle="0" color="red" color2="red" colors="8519f fuchsia;.25 #8000ff;24903f #0040ff;.5 aqua;41287f #00ff40;.75 #0bed00;57671f yellow"></v:fill>',
      '</v:rect>',
      '</DIV>'
    ].join('');

    picker = [
      '<DIV style="position: relative; width: 100%; height: 100%">',
      '<v:rect style="position: absolute; left: -1px; top: -1px; width: 101%; height: 101%" stroked="f" filled="t">',
      '<v:fill type="gradient" method="none" angle="270" color="#FFFFFF" opacity="100%" color2="#CC9A81" o:opacity2="0%"></v:fill>',
      '</v:rect>',
      '<v:rect style="position: absolute; left: 0px; top: 0px; width: 100%; height: 101%" stroked="f" filled="t">',
      '<v:fill type="gradient" method="none" angle="0" color="#000000" opacity="100%" color2="#CC9A81" o:opacity2="0%"></v:fill>',
      '</v:rect>',
      '</DIV>'
    ].join('');

    if (!document.namespaces['v'])
      document.namespaces.add('v', 'urn:schemas-microsoft-com:vml', '#default#VML');
  }

  /**
   * Convert HSV representation to RGB HEX string.
   * Credits to http://www.raphaeljs.com
   */
  function hsv2rgb(hsv) {
    var R, G, B, X, C;
    var h = (hsv.h % 360) / 60;

    C = hsv.v * hsv.s;
    X = C * (1 - Math.abs(h % 2 - 1));
    R = G = B = hsv.v - C;

    h = ~~h;
    R += [C, X, 0, 0, X, C][h];
    G += [X, C, C, X, 0, 0][h];
    B += [0, 0, X, C, C, X][h];

    var r = Math.floor(R * 255);
    var g = Math.floor(G * 255);
    var b = Math.floor(B * 255);
    return {r: r, g: g, b: b, hex: "#" + (16777216 | b | (g << 8) | (r << 16)).toString(16).slice(1)};
  }

  /**
   * Convert RGB representation to HSV.
   * r, g, b can be either in <0,1> range or <0,255> range.
   * Credits to http://www.raphaeljs.com
   */
  function rgb2hsv(rgb) {

    var r = rgb.r;
    var g = rgb.g;
    var b = rgb.b;

    if (rgb.r > 1 || rgb.g > 1 || rgb.b > 1) {
      r /= 255;
      g /= 255;
      b /= 255;
    }

    var H, S, V, C;
    V = Math.max(r, g, b);
    C = V - Math.min(r, g, b);
    H = (C == 0 ? null :
      V == r ? (g - b) / C + (g < b ? 6 : 0) :
        V == g ? (b - r) / C + 2 :
        (r - g) / C + 4);
    H = (H % 6) * 60;
    S = C == 0 ? 0 : C / V;
    return {h: H, s: S, v: V};
  }

  /**
   * Return click event handler for the slider.
   * Sets picker background color and calls ctx.callback if provided.
   */
  function slideListener(ctx, slideElement, pickerElement) {
    return function (evt) {
      evt = evt || window.event;
      var mouse = mousePosition(evt);
      ctx.h = mouse.x / slideElement.offsetWidth * 360 + hueOffset;
      var pickerColor = hsv2rgb({h: ctx.h, s: 1, v: 1});
      var c = hsv2rgb({h: ctx.h, s: ctx.s, v: ctx.v});
      pickerElement.style.backgroundColor = pickerColor.hex;
      ctx.callback && ctx.callback(c.hex, {h: ctx.h - hueOffset, s: ctx.s, v: ctx.v}, {r: c.r, g: c.g, b: c.b}, undefined, mouse);
    }
  };

  /**
   * Return click event handler for the picker.
   * Calls ctx.callback if provided.
   */
  function pickerListener(ctx, pickerElement) {
    return function (evt) {
      evt = evt || window.event;
      var mouse = mousePosition(evt),
        width = pickerElement.offsetWidth,
        height = pickerElement.offsetHeight;

      ctx.s = mouse.x / width;
      ctx.v = (height - mouse.y) / height;
      var c = hsv2rgb(ctx);
      ctx.callback && ctx.callback(c.hex, {h: ctx.h - hueOffset, s: ctx.s, v: ctx.v}, {r: c.r, g: c.g, b: c.b}, mouse);
    }
  };

  var uniqID = 0;

  /**
   * ColorPicker.
   * @param {DOMElement} slideElement HSV slide element.
   * @param {DOMElement} pickerElement HSV picker element.
   * @param {Function} callback Called whenever the color is changed provided chosen color in RGB HEX format as the only argument.
   */
  function ColorPicker(slideElement, pickerElement, callback) {

    if (!(this instanceof ColorPicker)) return new ColorPicker(slideElement, pickerElement, callback);

    this.h = 0;
    this.s = 1;
    this.v = 1;

    if (!callback) {
      // call of the form ColorPicker(element, funtion(hex, hsv, rgb) { ... }), i.e. the no-hassle call.

      var element = slideElement;
      element.innerHTML = colorpickerHTMLSnippet;

      this.slideElement = element.getElementsByClassName('slide')[0];
      this.pickerElement = element.getElementsByClassName('picker')[0];
      var slideIndicator = element.getElementsByClassName('slide-indicator')[0];
      var pickerIndicator = element.getElementsByClassName('picker-indicator')[0];

      ColorPicker.fixIndicators(slideIndicator, pickerIndicator);

      this.callback = function (hex, hsv, rgb, pickerCoordinate, slideCoordinate) {

        ColorPicker.positionIndicators(slideIndicator, pickerIndicator, slideCoordinate, pickerCoordinate);

        pickerElement(hex, hsv, rgb);
      };

    } else {

      this.callback = callback;
      this.pickerElement = pickerElement;
      this.slideElement = slideElement;
    }

    if (type == 'SVG') {

      // Generate uniq IDs for linearGradients so that we don't have the same IDs within one document.
      // Then reference those gradients in the associated rectangles.

      var slideClone = slide.cloneNode(true);
      var pickerClone = picker.cloneNode(true);

      var hsvGradient = slideClone.getElementById('gradient-hsv');

      var hsvRect = slideClone.getElementsByTagName('rect')[0];

      hsvGradient.id = 'gradient-hsv-' + uniqID;
      hsvRect.setAttribute('fill', 'url(#' + hsvGradient.id + ')');

      var blackAndWhiteGradients = [pickerClone.getElementById('gradient-black'), pickerClone.getElementById('gradient-white')];
      var whiteAndBlackRects = pickerClone.getElementsByTagName('rect');

      blackAndWhiteGradients[0].id = 'gradient-black-' + uniqID;
      blackAndWhiteGradients[1].id = 'gradient-white-' + uniqID;

      whiteAndBlackRects[0].setAttribute('fill', 'url(#' + blackAndWhiteGradients[1].id + ')');
      whiteAndBlackRects[1].setAttribute('fill', 'url(#' + blackAndWhiteGradients[0].id + ')');

      this.slideElement.appendChild(slideClone);
      this.pickerElement.appendChild(pickerClone);

      uniqID++;

    } else {

      this.slideElement.innerHTML = slide;
      this.pickerElement.innerHTML = picker;
    }

    addEventListener(this.slideElement, 'click', slideListener(this, this.slideElement, this.pickerElement));
    addEventListener(this.pickerElement, 'click', pickerListener(this, this.pickerElement));

    enableDragging(this, this.slideElement, slideListener(this, this.slideElement, this.pickerElement));
    enableDragging(this, this.pickerElement, pickerListener(this, this.pickerElement));
  };

  function addEventListener(element, event, listener) {

    if (element.attachEvent) {

      element.attachEvent('on' + event, listener);

    } else if (element.addEventListener) {

      element.addEventListener(event, listener, false);
    }
  }

  /**
   * Enable drag&drop color selection.
   * @param {object} ctx ColorPicker instance.
   * @param {DOMElement} element HSV slide element or HSV picker element.
   * @param {Function} listener Function that will be called whenever mouse is dragged over the element with event object as argument.
   */
  function enableDragging(ctx, element, listener) {

    var mousedown = false;

    addEventListener(element, 'mousedown', function (evt) {
      mousedown = true;
    });
    addEventListener(element, 'mouseup', function (evt) {
      mousedown = false;
    });
    addEventListener(element, 'mouseout', function (evt) {
      mousedown = false;
    });
    addEventListener(element, 'mousemove', function (evt) {

      if (mousedown) {

        listener(evt);
      }
    });
  }


  ColorPicker.hsv2rgb = function (hsv) {
    var rgbHex = hsv2rgb(hsv);
    delete rgbHex.hex;
    return rgbHex;
  };

  ColorPicker.hsv2hex = function (hsv) {
    return hsv2rgb(hsv).hex;
  };

  ColorPicker.rgb2hsv = rgb2hsv;

  ColorPicker.rgb2hex = function (rgb) {
    return hsv2rgb(rgb2hsv(rgb)).hex;
  };

  ColorPicker.hex2hsv = function (hex) {
    return rgb2hsv(ColorPicker.hex2rgb(hex));
  };

  ColorPicker.hex2rgb = function (hex) {
    return {r: parseInt(hex.substr(1, 2), 16), g: parseInt(hex.substr(3, 2), 16), b: parseInt(hex.substr(5, 2), 16)};
  };

  /**
   * Sets color of the picker in hsv/rgb/hex format.
   * @param {object} ctx ColorPicker instance.
   * @param {object} hsv Object of the form: { h: <hue>, s: <saturation>, v: <value> }.
   * @param {object} rgb Object of the form: { r: <red>, g: <green>, b: <blue> }.
   * @param {string} hex String of the form: #RRGGBB.
   */
  function setColor(ctx, hsv, rgb, hex) {
    ctx.h = hsv.h % 360;
    ctx.s = hsv.s;
    ctx.v = hsv.v;

    var c = hsv2rgb(ctx);

    var mouseSlide = {
      y: 0,   // not important
      x: (ctx.h * ctx.slideElement.offsetWidth) / 360
    };

    var pickerHeight = ctx.pickerElement.offsetHeight;

    var mousePicker = {
      x: ctx.s * ctx.pickerElement.offsetWidth,
      y: pickerHeight - ctx.v * pickerHeight
    };

    ctx.pickerElement.style.backgroundColor = hsv2rgb({h: ctx.h, s: 1, v: 1}).hex;
    ctx.callback && ctx.callback(hex || c.hex, {h: ctx.h, s: ctx.s, v: ctx.v}, rgb || {r: c.r, g: c.g, b: c.b}, mousePicker, mouseSlide);

    return ctx;
  };

  /**
   * Sets color of the picker in hsv format.
   * @param {object} hsv Object of the form: { h: <hue>, s: <saturation>, v: <value> }.
   */
  ColorPicker.prototype.setHsv = function (hsv) {
    return setColor(this, hsv);
  };

  /**
   * Sets color of the picker in rgb format.
   * @param {object} rgb Object of the form: { r: <red>, g: <green>, b: <blue> }.
   */
  ColorPicker.prototype.setRgb = function (rgb) {
    return setColor(this, rgb2hsv(rgb), rgb);
  };

  /**
   * Sets color of the picker in hex format.
   * @param {string} hex Hex color format #RRGGBB.
   */
  ColorPicker.prototype.setHex = function (hex) {
    return setColor(this, ColorPicker.hex2hsv(hex), undefined, hex);
  };

  /**
   * Helper to position indicators.
   * @param {HTMLElement} slideIndicator DOM element representing the indicator of the slide area.
   * @param {HTMLElement} pickerIndicator DOM element representing the indicator of the picker area.
   * @param {object} mouseSlide Coordinates of the mouse cursor in the slide area.
   * @param {object} mousePicker Coordinates of the mouse cursor in the picker area.
   */
  ColorPicker.positionIndicators = function (slideIndicator, pickerIndicator, mouseSlide, mousePicker) {

    if (mouseSlide) {
      slideIndicator.style.left = (mouseSlide.x - slideIndicator.offsetWidth / 2) + 'px';
    }
    if (mousePicker) {
      pickerIndicator.style.top = (mousePicker.y - pickerIndicator.offsetHeight / 2) + 'px';
      pickerIndicator.style.left = (mousePicker.x - pickerIndicator.offsetWidth / 2) + 'px';
    }
  };

  /**
   * Helper to fix indicators - this is recommended (and needed) for dragable color selection (see enabledDragging()).
   */
  ColorPicker.fixIndicators = function (slideIndicator, pickerIndicator) {

    pickerIndicator.style.pointerEvents = 'none';
    slideIndicator.style.pointerEvents = 'none';
  };

  window.ColorPicker = ColorPicker;

})(window, window.document);

/*
RainbowVis-JS 
Released under Eclipse Public License - v 1.0
*/

function Rainbow()
{
	"use strict";
	var gradients = null;
	var minNum = 0;
	var maxNum = 100;
	var colours = ['ff0000', 'ffff00', '00ff00', '0000ff']; 
	setColours(colours);
	
	function setColours (spectrum) 
	{
		if (spectrum.length < 2) {
			throw new Error('Rainbow must have two or more colours.');
		} else {
			var increment = (maxNum - minNum)/(spectrum.length - 1);
			var firstGradient = new ColourGradient();
			firstGradient.setGradient(spectrum[0], spectrum[1]);
			firstGradient.setNumberRange(minNum, minNum + increment);
			gradients = [ firstGradient ];
			
			for (var i = 1; i < spectrum.length - 1; i++) {
				var colourGradient = new ColourGradient();
				colourGradient.setGradient(spectrum[i], spectrum[i + 1]);
				colourGradient.setNumberRange(minNum + increment * i, minNum + increment * (i + 1)); 
				gradients[i] = colourGradient; 
			}

			colours = spectrum;
		}
	}

	this.setSpectrum = function () 
	{
		setColours(arguments);
		return this;
	}

	this.setSpectrumByArray = function (array)
	{
		setColours(array);
		return this;
	}

	this.colourAt = function (number)
	{
		if (isNaN(number)) {
			throw new TypeError(number + ' is not a number');
		} else if (gradients.length === 1) {
			return gradients[0].colourAt(number);
		} else {
			var segment = (maxNum - minNum)/(gradients.length);
			var index = Math.min(Math.floor((Math.max(number, minNum) - minNum)/segment), gradients.length - 1);
			return gradients[index].colourAt(number);
		}
	}

	this.colorAt = this.colourAt;

	this.setNumberRange = function (minNumber, maxNumber)
	{
		if (maxNumber > minNumber) {
			minNum = minNumber;
			maxNum = maxNumber;
			setColours(colours);
		} else {
			throw new RangeError('maxNumber (' + maxNumber + ') is not greater than minNumber (' + minNumber + ')');
		}
		return this;
	}
}

function ColourGradient() 
{
	"use strict";
	var startColour = 'ff0000';
	var endColour = '0000ff';
	var minNum = 0;
	var maxNum = 100;

	this.setGradient = function (colourStart, colourEnd)
	{
		startColour = getHexColour(colourStart);
		endColour = getHexColour(colourEnd);
	}

	this.setNumberRange = function (minNumber, maxNumber)
	{
		if (maxNumber > minNumber) {
			minNum = minNumber;
			maxNum = maxNumber;
		} else {
			throw new RangeError('maxNumber (' + maxNumber + ') is not greater than minNumber (' + minNumber + ')');
		}
	}

	this.colourAt = function (number)
	{
		return calcHex(number, startColour.substring(0,2), endColour.substring(0,2)) 
			+ calcHex(number, startColour.substring(2,4), endColour.substring(2,4)) 
			+ calcHex(number, startColour.substring(4,6), endColour.substring(4,6));
	}
	
	function calcHex(number, channelStart_Base16, channelEnd_Base16)
	{
		var num = number;
		if (num < minNum) {
			num = minNum;
		}
		if (num > maxNum) {
			num = maxNum;
		} 
		var numRange = maxNum - minNum;
		var cStart_Base10 = parseInt(channelStart_Base16, 16);
		var cEnd_Base10 = parseInt(channelEnd_Base16, 16); 
		var cPerUnit = (cEnd_Base10 - cStart_Base10)/numRange;
		var c_Base10 = Math.round(cPerUnit * (num - minNum) + cStart_Base10);
		return formatHex(c_Base10.toString(16));
	}

	function formatHex(hex) 
	{
		if (hex.length === 1) {
			return '0' + hex;
		} else {
			return hex;
		}
	} 
	
	function isHexColour(string)
	{
		var regex = /^#?[0-9a-fA-F]{6}$/i;
		return regex.test(string);
	}

	function getHexColour(string)
	{
		if (isHexColour(string)) {
			return string.substring(string.length - 6, string.length);
		} else {
			var name = string.toLowerCase();
			if (colourNames.hasOwnProperty(name)) {
				return colourNames[name];
			}
			throw new Error(string + ' is not a valid colour.');
		}
	}
	
	// Extended list of CSS colornames s taken from
	// http://www.w3.org/TR/css3-color/#svg-color
	var colourNames = {
		aliceblue: "F0F8FF",
		antiquewhite: "FAEBD7",
		aqua: "00FFFF",
		aquamarine: "7FFFD4",
		azure: "F0FFFF",
		beige: "F5F5DC",
		bisque: "FFE4C4",
		black: "000000",
		blanchedalmond: "FFEBCD",
		blue: "0000FF",
		blueviolet: "8A2BE2",
		brown: "A52A2A",
		burlywood: "DEB887",
		cadetblue: "5F9EA0",
		chartreuse: "7FFF00",
		chocolate: "D2691E",
		coral: "FF7F50",
		cornflowerblue: "6495ED",
		cornsilk: "FFF8DC",
		crimson: "DC143C",
		cyan: "00FFFF",
		darkblue: "00008B",
		darkcyan: "008B8B",
		darkgoldenrod: "B8860B",
		darkgray: "A9A9A9",
		darkgreen: "006400",
		darkgrey: "A9A9A9",
		darkkhaki: "BDB76B",
		darkmagenta: "8B008B",
		darkolivegreen: "556B2F",
		darkorange: "FF8C00",
		darkorchid: "9932CC",
		darkred: "8B0000",
		darksalmon: "E9967A",
		darkseagreen: "8FBC8F",
		darkslateblue: "483D8B",
		darkslategray: "2F4F4F",
		darkslategrey: "2F4F4F",
		darkturquoise: "00CED1",
		darkviolet: "9400D3",
		deeppink: "FF1493",
		deepskyblue: "00BFFF",
		dimgray: "696969",
		dimgrey: "696969",
		dodgerblue: "1E90FF",
		firebrick: "B22222",
		floralwhite: "FFFAF0",
		forestgreen: "228B22",
		fuchsia: "FF00FF",
		gainsboro: "DCDCDC",
		ghostwhite: "F8F8FF",
		gold: "FFD700",
		goldenrod: "DAA520",
		gray: "808080",
		green: "008000",
		greenyellow: "ADFF2F",
		grey: "808080",
		honeydew: "F0FFF0",
		hotpink: "FF69B4",
		indianred: "CD5C5C",
		indigo: "4B0082",
		ivory: "FFFFF0",
		khaki: "F0E68C",
		lavender: "E6E6FA",
		lavenderblush: "FFF0F5",
		lawngreen: "7CFC00",
		lemonchiffon: "FFFACD",
		lightblue: "ADD8E6",
		lightcoral: "F08080",
		lightcyan: "E0FFFF",
		lightgoldenrodyellow: "FAFAD2",
		lightgray: "D3D3D3",
		lightgreen: "90EE90",
		lightgrey: "D3D3D3",
		lightpink: "FFB6C1",
		lightsalmon: "FFA07A",
		lightseagreen: "20B2AA",
		lightskyblue: "87CEFA",
		lightslategray: "778899",
		lightslategrey: "778899",
		lightsteelblue: "B0C4DE",
		lightyellow: "FFFFE0",
		lime: "00FF00",
		limegreen: "32CD32",
		linen: "FAF0E6",
		magenta: "FF00FF",
		maroon: "800000",
		mediumaquamarine: "66CDAA",
		mediumblue: "0000CD",
		mediumorchid: "BA55D3",
		mediumpurple: "9370DB",
		mediumseagreen: "3CB371",
		mediumslateblue: "7B68EE",
		mediumspringgreen: "00FA9A",
		mediumturquoise: "48D1CC",
		mediumvioletred: "C71585",
		midnightblue: "191970",
		mintcream: "F5FFFA",
		mistyrose: "FFE4E1",
		moccasin: "FFE4B5",
		navajowhite: "FFDEAD",
		navy: "000080",
		oldlace: "FDF5E6",
		olive: "808000",
		olivedrab: "6B8E23",
		orange: "FFA500",
		orangered: "FF4500",
		orchid: "DA70D6",
		palegoldenrod: "EEE8AA",
		palegreen: "98FB98",
		paleturquoise: "AFEEEE",
		palevioletred: "DB7093",
		papayawhip: "FFEFD5",
		peachpuff: "FFDAB9",
		peru: "CD853F",
		pink: "FFC0CB",
		plum: "DDA0DD",
		powderblue: "B0E0E6",
		purple: "800080",
		red: "FF0000",
		rosybrown: "BC8F8F",
		royalblue: "4169E1",
		saddlebrown: "8B4513",
		salmon: "FA8072",
		sandybrown: "F4A460",
		seagreen: "2E8B57",
		seashell: "FFF5EE",
		sienna: "A0522D",
		silver: "C0C0C0",
		skyblue: "87CEEB",
		slateblue: "6A5ACD",
		slategray: "708090",
		slategrey: "708090",
		snow: "FFFAFA",
		springgreen: "00FF7F",
		steelblue: "4682B4",
		tan: "D2B48C",
		teal: "008080",
		thistle: "D8BFD8",
		tomato: "FF6347",
		turquoise: "40E0D0",
		violet: "EE82EE",
		wheat: "F5DEB3",
		white: "FFFFFF",
		whitesmoke: "F5F5F5",
		yellow: "FFFF00",
		yellowgreen: "9ACD32"
	}
}

if (typeof module !== 'undefined') {
  module.exports = Rainbow;
}
