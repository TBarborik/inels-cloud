(function () {
  'use strict';

  google.charts.load("current", {packages: ["corechart"]});

  var app = angular.module('application', [
      'ui.router',
      'ngAnimate',
      'pascalprecht.translate',
      'ngWebSocket',

      //foundation
      'foundation',
      'foundation.dynamicRouting',
      'foundation.dynamicRouting.animations',

      //custom
      'Authentication',
      'Rooms'
    ])
    .factory('loginChecker', ["$q", "$injector", function ($q, $injector) {
      var loginChecker = {};

      loginChecker.responseError = function (response) {
        if (response.status == "401") {
          var AuthService = $injector.get('AuthService');
          AuthService.logOut(function () {
          });
        }
      };

      return loginChecker;
    }])
    .config(config)
    .run(run);

  config.$inject = ['$urlRouterProvider', '$locationProvider', '$translateProvider', "$httpProvider"];

  function config($urlProvider, $locationProvider, $translateProvider, $httpProvider) {
    $httpProvider.interceptors.push('loginChecker');

    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });

    $translateProvider.translations('en', {
      'LIVING_ROOM': 'Living Room',
      'BED_ROOM': 'Bed Room',
      'KITCHEN': 'Kitchen',
      'HALL': 'Hall',
      "up": "UP",
      "down": "DOWN",
      "device": "DEVICE"
    });

    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.preferredLanguage('en');
  }

  function run($rootScope, $location, $state, AuthService, RoomService) {
    FastClick.attach(document.body);

    $rootScope.loggedIn = AuthService.isLoggedIn();
    if ($rootScope.loggedIn)
      $rootScope.user = AuthService.getUser();

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams) {
      if (typeof toState.data.vars.requireLogin !== "undefined" && toState.data.vars.requireLogin && (typeof $rootScope.loggedIn === "undefined" || !$rootScope.loggedIn)) {
        event.preventDefault();
        $state.go("entry");
      }
    });

    $rootScope.logout = function () {
      AuthService.logOut(function () {
      });
    };

    $rootScope.testUrl = function ($testing) {
      return $state.current.name.indexOf($testing) > -1;
    };

    $rootScope.statusBarDevices = [];
    $rootScope.statusBarActive = RoomService.actual;
    $rootScope.changeStatusIndex = function (i) {
      $rootScope.statusBarActive = i;
      RoomService.actual = i;
      $state.reload();
    };
    RoomService.getStatusDevices(function (response) {
      $rootScope.statusBarDevices = response.data;
    });
  }

  /** CONTROLLERS **/
  /** CAN BE CHANGED! change it as you need, this is only for demonstration. **/

  app.controller("EntryController", function ($scope, AuthService, $state, $rootScope) {

    if ($rootScope.loggedIn)
      $state.go("devices/dashboard");

    $scope.formChooser = true;
    $scope.registerData = {
      "username": "",
      "password": "",
      "passwordCheck": "",
      "email": "",
      "agree": false
    };

    $scope.loginData = {
      "username": "",
      "password": ""
    };

    $scope.login = function () {
      AuthService.logIn($scope.loginData.username, $scope.loginData.password, function () {
        $rootScope.loggedIn = true;
        $rootScope.user = AuthService.getUser();
        $state.go("devices/dashboard");
      }, function () {
        $scope.unsuccessfulLogin = true;
      });
    }

  });

  app.controller("DevicesController", ["$scope", "$rootScope", "$state", "RoomService", function ($scope, $rootScope, $state, RoomService) {
    $scope.currentState = $state;

    $scope.dashboardDevices = [
      {
        name: "Light",
        room: "Kitchen",
        status: false,
        favourite: true
      },
      {
        name: "Blinds",
        room: "Living room",
        status: true,
        favourite: false
      }
    ];

    $scope.intercomDevices = [
      {
        name: "Thomas",
        device: "iPAD",
        phone: "CZ 777 027 229"
      },
      {
        name: "Jane",
        device: "intercom",
        phone: "-"
      }
    ];
    $scope.dashboardOrder = "name";
    $scope.intercomOrder = "name";
    $scope.intercomEdit = -1;
    $scope.rangeTest = 15;
    $scope.selectedCamera = -1;
    $scope.val = 2;

    $scope.selectCamera = function (i) {
      if ($scope.selectedCamera != i)
        $scope.selectedCamera = i;
    };

    $scope.energy = function () {
      google.charts.setOnLoadCallback(drawDonutChart);

      function drawDonutChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['1', 49],
          ['2', 14],
          ['3', 37],
        ]);

        var options = {
          pieHole: 0.4,
          colors: ["#8796A0", "#19B4FF", "#F1C925"],
          legend: "none",
          chartArea: {height: "90%"}
        };

        var chart = new google.visualization.PieChart(document.getElementById('graph-donut'));
        chart.draw(data, options);
      }
    };

    $scope.selectItems = {
      0: "Day",
      1: "Yesterday",
      2: "7 Days",
      3: "30 Days",
      4: "This month",
      5: "Last month",
      6: "Period"
    };
    $scope.selectedItem = 1;

    // ukázka práce s kalendářem (nutné dělat přes scope aby se projevily změny)
    $scope.calendarActive = "2016-7-10,2016-7-12";
    $scope.calendarClick = function (d) {
      var cal = angular.element(document.querySelector("il-calendar"));
      var pos = $scope.calendarActive.split(",").indexOf(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());

      if (pos > -1) {
        var splitted = $scope.calendarActive.split(",");
        splitted.splice(pos, 1);
        if (!$rootScope.$$phase)
          $scope.$apply(function () {
            $scope.calendarActive = splitted.join(",");
          });
        return;
      }


      var t = $scope.calendarActive.split(",");
      if (t.length >= 2) {
        t.pop();
      }

      t.push(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());

      if (!$rootScope.$$phase)
        $scope.$apply(function () {
          $scope.calendarActive = t.join(",");
        });
    };

    $scope.calendarMonth = "2016-8";

    $scope.calendarMonthChange = function (m) {
      //console.log(m);
    };

    $scope.selectFunc = function (name, key) {
      //console.log("položka změněna");
    };

    $scope.cards = {
      light: true,
      lamp: true,
      hall: false
    };
    $scope.switchChange = function (name, value) {
      switch (name) {
        case "light":
          $scope.cards.light = value;
          break;
        case "lamp":
          $scope.cards.lamp = value;
          break;
        case "hall":
          $scope.cards.hall = value;
          break;
      }
    };

    $scope.hourData = {
      '2016-11-04-01': 5414,
      '2016-11-08-02': 2014,
      '2016-11-08-15': 3921
    };

    $scope.testVar = "as";

    $scope.lineData = [
      {0: 0, 0.25: 352, 1: 652, 2: 462, 3: 215, 4: 952, 5: 16, 6: 0, 7.16: 1000, 8: 365, 9: 412, 10: 548, 11: 258, 12: 258, 15: 651, 16: 25, 17: 69, 18: 78, 19: 458, 19.58: 920, 20: 352, 21: 245, 22: 852, 23: 756, 24: 352},
      {
        0: 0,
        0.58: 756,
        1.2: 279,
        2: 145,
        3: 495,
        4: 916,
        5: 735,
        6.7: 290,
        7: 358,
        8: 959,
        9.5: 408,
        10: 533,
        11.8: 374,
        13: 499,
        14: 614,
        15: 664,
        16: 952,
        17: 907,
        18: 990,
        19.33: 832,
        19.5: 22,
        20: 682,
        20.6: 963,
        21: 725,
        22: 430,
        22.42: 581,
        23: 317,
        23.5: 505,
        24: 132
      },
      {
        0: 500,
        1: 500,
        2: 500,
        3: 500,
        4: 500,
        5: 500,
        6: 500,
        7: 500,
        8: 500,
        9: 500,
        10: 500,
        11: 500,
        12: 500,
        13: 500,
        14: 500,
        15: 500,
        16: 500,
        17: 500,
        18: 500,
        19: 500,
        20: 500,
        21: 500,
        22: 500,
        23: 500,
        24: 500,
      }
    ];

    $scope.lineDataChange = function () {
      $scope.lineData = [
        {0: 0, 0.75: 252, 1: 552, 2: 462, 3: 25, 4: 902, 5: 162, 6: 0, 7.16: 0, 8: 35, 9.25: 812, 10: 548, 11: 258, 12: 258, 15: 651, 16: 25, 17: 69, 18: 78, 19: 458, 19.58: 920, 20: 352, 21: 245, 22: 852, 23: 756, 24: 352},
        {
          0: 500,
          1: 500,
          2: 500,
          3: 500,
          4: 500,
          5: 500,
          6: 500,
          7: 500,
          8: 500,
          9: 500,
          10: 500,
          11: 500,
          12: 500,
          13: 500,
          14: 500,
          15: 500,
          16: 500,
          17: 500,
          18: 500,
          19: 500,
          20: 500,
          21: 500,
          22: 500,
          23: 500,
          24: 500,
        }
      ];
    }

    $scope.lineLabels = ["yesterday", "today", "average"];

    $scope.counterChangeFn = function (name, val) {
      //console.log(name + " | " + val);
    }

    $scope.colorpickerChangeFn = function (name, val) {
      //console.log(name + " | " + val);
    }

  }]);

  app.controller("DashboardController", ["$scope", "RoomService", "$websocket", function ($scope, RoomService, $websocket) {
    $scope.deviceList = [];
    $scope.dashboardOrder = "order";
    $scope.greaterThan = function (prop, val) {
      return function (item) {
        return item[prop] > val;
      }
    };

    RoomService.getRoomsDevices(function (devices) {
      $scope.deviceList = $scope.deviceList.concat(devices);
    });

    var dataStream = $websocket("wss://www.elkoep.cloud/wssapp/v1");
    dataStream.onOpen(function () {
      console.log("Connection established.");
    });
    dataStream.onClose(function () {
      console.log("Connection closed.");
    });
    dataStream.onMessage(function (r) {
      RoomService.alter($scope.deviceList, JSON.parse(r.data));
    })
  }]);

  app.controller("RoomsController", ["$scope", "RoomService", "$stateParams", "$rootScope", "$websocket", "$state", function ($scope, RoomService, $stateParams, $rootScope, $websocket, $state) {
    $scope.detail = !(typeof($stateParams.roomName) === "undefined" || $stateParams.roomName === "");
    $scope.deviceList = [];

    // switch reaction
    $scope.switched = function (name, value, item, type) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      var address, state;

      switch (type) {
        case "rgba" :
          address = "rgba";
          state = {
            state: [
              device[0].addresses.R, device[0].colors.r / 2.55,
              device[0].addresses.G, device[0].colors.g / 2.55,
              device[0].addresses.B, device[0].colors.b / 2.55,
              device[0].addresses.A, value ? device[0].states.A_old : 0
            ]
          };
          break;

        default:
          address = device[0].address;
          state = {"state": value ? 1 : 0};
      }

      RoomService.change(address, state, function (r) {
        //console.log("as");
      });
    };

    // blinds change
    $scope.changeBlinds = function (device, dir) {
      var address = false, state = false;

      switch (dir) {
        case "up":
          if (device.states.down == 1) { // down is one => send zero to down
            state = {state: 0};
            address = device.addresses.down;
          } else if (device.states.up == 0) { // down is zero and up as well => send one to up
            state = {state: 1};
            address = device.addresses.up;
          }
          break;
        case "down":
          if (device.states.up == 1) { // up is one => change state of up to zero on address of up
            state = {state: 0};
            address = device.addresses.up;
          } else if (device.states.down == 0) { // up is zero and down as well => change state of down to 1 on address of down
            address = device.addresses.down;
            state = {state: 1};
          }
          break;
      }

      if (address !== false && state !== false)
        RoomService.change(address, state, function (r) {
        });
    };

    // range change (dimmer)
    $scope.ranged = function (name, value) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      RoomService.change(device[0].address, {"state": value}, function (r) {

      });
    };

    $scope.changeColorPicker = function (name, color) {
      var device = $scope.deviceList.filter(function (item) {
        return item.dev_name == name;
      });

      device = device[0];

      var states = {
        state: [
          device.addresses.R, color.r / 2.55,
          device.addresses.G, color.g / 2.55,
          device.addresses.B, color.b / 2.55,
          device.addresses.A, device.states.A
        ]
      };

      RoomService.change("rgba", states, function (r) {
      });
    };

    // layout switch
    if ($scope.detail) { // detail of rooms with cards
      $scope.room = $stateParams.roomName;
      $scope.i = 0;

      RoomService.get(true, $stateParams.roomName, function (data) {
        $scope.deviceList = data;
      });

      // websockets
      var dataStream = $websocket("wss://www.elkoep.cloud/wssapp/v1");
      dataStream.onOpen(function () {
        console.log("Connection established.");
      });
      dataStream.onClose(function () {
        console.log("Connection closed.");
      });
      dataStream.onMessage(function (r) {
        $scope.deviceList = RoomService.alter($scope.deviceList, JSON.parse(r.data), "addresses");
      })
    } else { // list of rooms
      RoomService.getRooms(function (r) {
        var keys = Object.keys(r.data);
        if (keys.length == 1) {
          $state.go("devices/rooms", {roomName: r.data[keys[0]]});
        }
        $scope.rooms = r.data;

      }), function (r) {
        //console.log(r);
      };
    }
  }]);

  /** DIRECTIVES **/
  /** DO NOT CHANGE! unless it's really necessary. **/

  app.directive('passwordValidation', ['$parse', function ($parse) {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ctrl) {
        scope.$watch(function () { // matching validation
          return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) || $parse(attrs.passwordValidation)(scope) === ctrl.$modelValue;
        }, function (currentValue) {
          ctrl.$setValidity('match', currentValue);
        });

        ctrl.$parsers.unshift(function (viewValue) { // strength (content) validation
          var pwdValidLength, pwdHasULetter, pwdHasLLetter, pwdHasNumber, pwdHasSpecial;

          pwdValidLength = (viewValue && viewValue.length >= 8) ? true : false;
          pwdHasULetter = (viewValue && /[A-Z]/.test(viewValue)) ? true : false;
          pwdHasLLetter = (viewValue && /[a-z]/.test(viewValue)) ? true : false;
          pwdHasNumber = (viewValue && /\d/.test(viewValue)) ? true : false;
          pwdHasSpecial = (viewValue && /[^a-zA-Z0-9]/.test(viewValue)) ? true : false;

          ctrl.$setValidity('pwd', pwdValidLength && pwdHasULetter && pwdHasLLetter && pwdHasNumber && pwdHasSpecial);
          return viewValue;
        });
      },
    };
  }]);


  app.directive("ilHeader", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/site/header.html"
    }
  });

  app.directive("ilFooter", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/site/footer.html"
    }
  });

  app.directive("ilStatusBar", function (RoomService) {
    return {
      restrict: "E",
      templateUrl: "./templates/devices/status_bar.html"
    }
  });

  app.directive("ilDevicesNavigation", function () {
    return {
      restrict: "E",
      templateUrl: "./templates/devices/navigation.html"
    }
  });

  app.directive("ilSwitch", function () {
    return {
      restrict: "E",
      scope: {
        name: "@",
        checked: "=",
        changeFn: "=",
        ngModel: "=",
        item: "=",
        type: "@",
        disabled: "="
      },
      link: function (scope, element, attrs) {

        // tried change state
        element.find("label").on("click", function (e) {
          if (typeof(scope.disabled) !== "undefined" && scope.disabled) {
            return;
          }

          if (typeof(scope.ngModel !== "undefined")) {
            scope.ngModel = !scope.switchbox;
          }

          if (typeof(scope.changeFn) !== "undefined") {
            scope.changeFn(scope.name, !scope.switchbox, scope.item, scope.type);
          }
        });

        // if model is not undefined, set default value
        if (typeof scope.ngModel != "undefined") {
          scope.switchbox = scope.ngModel >= 1 || scope.ngModel;
        }
        else if (typeof scope.checked != "undefined")
          scope.switchbox = scope.checked;

        if (typeof(scope.ngModel) !== "undefined") {
          scope.$watch("ngModel", function (nValue) {
            scope.switchbox = nValue >= 1 || nValue;
          })
        }
      }

      ,
      template: '<input type="checkbox" name="{{ name }}" id="switch-{{ name }}" ng-model="switchbox" disabled="disabled"><label for="switch-{{ name }}"></label>'
    }
  });

  app.directive("ilRange", function () {
    return {
      restrict: "E",
      scope: {
        name: "@",
        value: "=",
        changeFn: "=",
        ngModel: "=",
      },
      link: function (scope, element, attrs) {

        element.find("input").on("change", function (e) {
          if (typeof(scope.ngModel !== "undefined")) {
            scope.ngModel = scope.range;
          }

          if (typeof(scope.changeFn) !== "undefined") {
            scope.changeFn(scope.name, scope.range);
          }
        });

        if (typeof scope.ngModel != "undefined") {
          scope.range = !isNaN(scope.ngModel) && Number.isInteger(scope.ngModel) ? scope.ngModel * 100 : scope.ngModel;
        }
        else if (typeof scope.checked != "undefined")
          scope.range = scope.value;

        if (typeof(scope.ngModel) !== "undefined") {
          scope.$watch("ngModel", function (nValue) {
            scope.range = !isNaN(nValue) && Number.isInteger(nValue) ? nValue * 100 : nValue;
          })
        }
      },
      template: '<input type="range" min=0 max=100 ng-model="ngModel" name="{{name}}" id="range-{{name}}" value="0">'
    }
  });

  app.directive("ilCard", function () {
    return {
      restrict: "CA",
      link: function (scope, element, attrs) {
        var opener = angular.element(element[0].querySelector("a.open"));


        opener.on("click", function (e) {
          e.preventDefault();

          if (element.hasClass("opened")) {
            element.removeClass("opened");
            element.addClass("closed");
          } else {
            element.removeClass("closed");
            element.addClass("opened");
          }
        });
      }
    }
  });

  app.directive("ilCounter", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        scope.ilnumber = (typeof attrs.value == "undefined") ? 0 : parseInt(attrs.value);
        var input = angular.element(element[0].querySelector("input"));
        var plus = angular.element(element[0].querySelector(".plus"));
        var minus = angular.element(element[0].querySelector(".minus"));
        var changeElement = null;
        input.attr("name", attrs.name);

        function changeValueIn(val) {
          changeElement.text(val);
        }

        if (typeof attrs.changeIn != "undefined") {
          changeElement = angular.element(document.querySelector(attrs.changeIn));
        }

        scope.$watch("ilnumber", function (nValue) {
          if (changeElement != null) {
            element.attr("value", nValue);
            changeValueIn(nValue);
          }

          if (typeof scope.changeFn != "undefined") {
            scope.changeFn(attrs.name, nValue);
          }
        });

        plus.on("click", function (e) {
          if (!scope.$$phase)
            scope.$apply(function () {
              scope.ilnumber++;
            });
        });

        minus.on("click", function (e) {
          if (!scope.$$phase)
            scope.$apply(function () {
              scope.ilnumber--;
            });
        });
      },
      scope: {
        changeFn: "=",
      },
      template: '<input type="number" ng-model="ilnumber" name=""><span class="minus">-</span><span class="plus">+</span>'
    };
  });

  app.directive("ilColorpicker", function () {
    return {
      restrict: "E",
      scope: {
        changeFn: "=",
        value: "="
      },
      link: function (scope, element, attrs) {
        var input = angular.element(element[0].querySelector("input"));
        var opener = angular.element(element[0].querySelector("a.opener"));
        var color_indicator = angular.element(element[0].querySelector(".color-indicator"));
        var picker = angular.element(element[0].querySelector(".picker-wrapper"));
        var slider = angular.element(element[0].querySelector(".color-slider"));


        ColorPicker.fixIndicators(element[0].querySelector(".slider-indicator"), element[0].querySelector('.picker-indicator'));
        var col = ColorPicker(element[0].querySelector(".color-slider"), element[0].querySelector(".color-picker"), function (hex, hsv, rgb, cursorPicker, cursorSlider) {
          input.attr("value", hex);
          color_indicator[0].style.backgroundColor = hex;
          scope.hex = hex;
          scope.rgb = rgb;

          ColorPicker.positionIndicators(
            element[0].querySelector(".slider-indicator"),
            element[0].querySelector('.picker-indicator'),
            cursorSlider, cursorPicker
          );
        });

        function changeIn() {
          scope.value = scope.rgb;
          if (typeof attrs.changeIn != "undefined") {
            angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
          }

          if (typeof scope.changeFn != "undefined") {
            scope.changeFn(attrs.name, scope.rgb);
          }
        }

        if (typeof scope.value != "undefined") {
          scope.actualValue = {};
          col.setRgb(scope.value);
          angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
          angular.copy(scope.value, scope.actualValue);
          color_indicator[0].style.backgroundColor = "rgb(" + scope.value.r + ", " + scope.value.g + ", " + scope.value.b + ")";

          setInterval(function () {
            if (!angular.equals(scope.value, scope.actualValue)) {
              col.setRgb(scope.value);
              angular.element(document.querySelector(attrs.changeIn)).text(scope.hex);
              angular.copy(scope.value, scope.actualValue);
            }
          }, 1000);
        } else {
          input.attr("value", "#FFFFFF");
          col.setHex("#FFFFFF");
        }

        if (typeof attrs.name != "undefined") {
          input.attr("name", attrs.name);
        }

        picker.on("click", function (e) {
          changeIn();
        });

        slider.on("click", function (e) {
          changeIn();
        });

        opener.on("click", function (event) {
          event.preventDefault();

          if (picker.hasClass("opened")) {
            picker.removeClass("opened");
            picker.addClass("closed");
          } else {
            picker.removeClass("closed");
            picker.addClass("opened");
          }
        });
      },
      template: '<input type="color" name=""><div class="grid-block"><div class="grid-content"><div class="slider-wrapper"><span class="slider-indicator"></span><div class="color-slider"></div></div></div><div class="grid-content shrink"><a href="#" class="color-indicator opener"></a><div class="picker-wrapper closed"><span class="picker-indicator"></span><div class="color-picker"></div></div></div></div>'
    }
  });

  app.directive("ilScale", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        var min = parseInt(attrs.min);
        var max = parseInt(attrs.max);
        var step = parseInt(attrs.step);
        var active = typeof attrs.active == "undefined" ? null : parseInt(attrs.active);
        var postfix = typeof attrs.postfix == "undefined" ? "" : attrs.postfix;
        var prefix = typeof attrs.prefix == "undefined" ? "" : attrs.prefix;

        var colorStart = typeof attrs.colorStart == "undefined" ? null : attrs.colorStart;
        var colorEnd = typeof attrs.colorEnd == "undefined" ? null : attrs.colorEnd;
        var color = null;
        var gradient = null;

        if (colorStart == colorEnd && colorStart != null) {
          color = colorStart;
        } else if (colorStart != null || colorEnd != null) {
          if (colorStart == null)
            colorStart = "#8796A0";
          else if (colorEnd == null)
            colorEnd = "#8796A0";

          gradient = new Rainbow;
          gradient.setNumberRange(min, max);
          gradient.setSpectrum(colorStart, colorEnd);
        }

        for (var i = min; i <= max; i += step) {
          var item = angular.element('<span class="item ' + ((i == active) ? "active" : "") + '" value="' + i + '">' + prefix + i + postfix + '</span>');
          if (color != null)
            item[0].style.color = color;

          if (gradient != null) {
            item[0].style.color = "#" + gradient.colourAt(i);
          }

          element.append(item);
        }
      },
      template: ""
    }
  });

  app.directive("color", function () {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element[0].style.color = attrs.color;
      }
    }
  });

  app.directive("ilMeter", function () {
    return {
      restrict: "E",
      link: function (scope, element, attrs) {
        element[0].style.visibility = "hidden";
        var min = parseInt(scope.min);
        var max = parseInt(scope.max);
        var points = 8; // + 0 = 9 points
        var step = (Math.abs(max - min) / points);


        for (var i = min, j = 0; i <= max; i += step, j++) {
          var unit = angular.element('<span class="unit u-' + j + '">' + Math.round(i * 100) / 100 + '</span>');
          element.prepend(unit);
        }

        element[0].style.visibility = "visible";

      },
      controller: function ($scope, $element, $attrs) {
        function set_position(val) {
          $element[0].querySelector(".pointer").style.top = (1 - (val - $scope.min) / Math.abs($scope.max - $scope.min)) * 100 + "%";
        }

        set_position($scope.value);
      },
      scope: {
        value: "@",
        min: "@",
        max: "@"
      },
      template: '<span class="pointer"></span><span class="meter"></span>'
    }
  });

  app.directive("responsiveScroller", function () {
    return {
      restrict: "AC",
      link: function ($scope, $element, $attrs) {

        var padding = "16";

        function changeSize() {
          $element[0].style.maxWidth = (document.body.clientWidth - padding * 2) + "px";
        }

        if (typeof $attrs.minWidth != "undefined") {
          $element.children()[0].style.minWidth = $attrs.minWidth + "px";
          $element.children()[0].style.display = "block";
        }

        changeSize();
        angular.element(window).on("resize", changeSize);
      }
    }
  });

  /**
   * After day selection calls function given in attribute click-fn
   * example: click-fn="calendarClick" calls function calendarClick
   */
  app.directive("ilCalendar", function () {
    return {
      restrict: "E",
      scope: {
        clickFn: "=",
        monthChangeFn: "="
      },
      link: function ($scope, $element, $attrs) {
        var weekDays = ["S", "M", "T", "W", "T", "F", "S"];
        var activeClasses = ["bg-blue", "bg-red"];
        $scope.clicked = -1;
        var gMonth = null;
        var monthNames = ["January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];

        function toDate(str) {
          if (typeof str != "string")
            return str;

          var parts = str.replace(" ", "").split("-").map(function (i) {
            return parseInt(i)
          });

          var date = new Date();
          for (var i = 0; i < parts.length; i++) {
            switch (i) {
              case 0:
                date.setFullYear(parts[i]);
                break;
              case 1:
                date.setMonth(parts[i] - 1);
                break;
              case 2:
                date.setDate(parts[i]);
                break;
              case 3:
                date.setHours(parts[i]);
                break;
              case 4:
                date.setMinutes(parts[i]);
                break;
            }
          }

          return date;
        }

        function render(month) {
          $element.html("");
          $element.attr("month", month.getFullYear() + "-" + (month.getMonth() + 1));
          var monthDuplicate = new Date(month.getFullYear(), month.getMonth(), month.getDate());
          gMonth = new Date(month.getFullYear(), month.getMonth(), month.getDate());
          monthDuplicate.setDate(0);
          var days = monthDuplicate.getDate();
          month.setDate(1);

          var months = angular.element("<table class='month-list'><tr><td class='previous'></td><td class='current'></td><td class='next'></td></tr></table>");
          var table = angular.element("<table></table>");
          var thead = angular.element("<thead><tr></tr></thead>");
          var tbody = angular.element("<tbody><tr></tr></tbody>");

          angular.element(months[0].querySelector(".current")).text(monthNames[month.getMonth()]);
          monthDuplicate = new Date(month.getFullYear(), month.getMonth() - 1, month.getDate());
          var prev = angular.element(months[0].querySelector(".previous")).text(monthNames[monthDuplicate.getMonth()]);
          monthDuplicate = new Date(month.getFullYear(), month.getMonth() + 1, month.getDate());
          var next = angular.element(months[0].querySelector(".next")).text(monthNames[monthDuplicate.getMonth()]);


          prev.on("click", function (event) {
            event.preventDefault();
            month = toDate($attrs.month);
            month.setMonth(month.getMonth() - 1);
            $attrs.month = month.getFullYear() + "-" + (month.getMonth() + 1);
            if (typeof $scope.monthChangeFn != "undefined") {
              $scope.monthChangeFn(month);
            }
            startRendering($attrs.month);
          });

          next.on("click", function (event) {
            event.preventDefault();
            month = toDate($attrs.month);
            month.setMonth(month.getMonth() + 1);
            $attrs.month = month.getFullYear() + "-" + (month.getMonth() + 1);
            if (typeof $scope.monthChangeFn != "undefined") {
              $scope.monthChangeFn(month);
            }
            startRendering($attrs.month);
          });

          table.append(thead);
          table.append(tbody);

          thead = thead.find("tr");
          tbody = tbody.find("tr");
          for (var i = 1; i <= days; i++) {
            month.setDate(i);
            var th = angular.element("<th>" + weekDays[month.getDay()] + "</th>");
            var td = angular.element("<td class='day-" + i + "'><span class='day' value='" + i + "'>" + i + "</span></td>");

            var span = td.find("span");
            span.on("click", function (event) {
              var clickedItem = angular.element(this);
              $scope.clicked = parseInt(clickedItem.attr("value"));
              $element.attr("last-clicked", $scope.clicked);

              if (typeof $scope.clickFn != "undefined")
                $scope.clickFn(new Date(month.getFullYear(), month.getMonth(), clickedItem.attr("value")));
            });

            if (month.getDay() == 0 || month.getDay() == 6)
              td.addClass("color-blue");

            thead.append(th);
            tbody.append(td);
          }

          $element.append(months);
          $element.append(table);

          if (typeof $attrs.active != "undefined")
            activeReact($attrs.active);
        }

        function startRendering(dateStr) {
          gMonth = toDate(dateStr);
          render(toDate(dateStr));
        }

        function activeReact(activeStr) {

          var actives = activeStr.replace(" ", "").split(",").map(function (i) {
            return toDate(i)
          });

          setActive(gMonth, actives);
        }

        function setActive(month, actives) {
          for (var i = 0; i < activeClasses.length; i++) {
            angular.element($element[0].querySelectorAll("td")).removeClass(activeClasses[i]);
          }

          for (var i = 0; i < actives.length; i++) {
            if (month.getFullYear() == actives[i].getFullYear() && month.getMonth() == actives[i].getMonth()) {
              angular.element($element[0].querySelector("td.day-" + actives[i].getDate())).addClass(activeClasses[i % activeClasses.length]);
            }
          }
        }

        $attrs.$observe("month", function (dateStr) {
          startRendering(dateStr);
        });

        $attrs.$observe("active", function (activeStr) {
          activeReact(activeStr);
        });
      }
    }
  });

  /**
   * After select calls function given in attribute select-fn
   * example: select-fn="selectFunc" calls function selectFunc with attribute selected value
   */
  app.directive("ilSelect", function () {
    return {
      restrict: "E",
      link: function ($scope, $element, $attrs) {

        var select = angular.element($element[0].querySelector("select"));
        var aItem = angular.element($element[0].querySelector(".selected-item"));
        var items = angular.element($element[0].querySelector(".items"));

        $scope.selected = null;

        if (typeof $attrs.selectedItem != "undefined")
          $scope.selected = Object.keys($scope.items)[0];

        select.attr("name", $scope.name);

        function render() {
          for (var key in Object.keys($scope.items)) {
            select.append('<option value="' + key + '">' + $scope.items[key] + '</option>');
            var item = angular.element('<div class="item" key="' + key + '">' + $scope.items[key] + '</div>');

            item.on("click", function (e) {
              $scope.selected = angular.element(this).attr("key");
              selected();
            });

            items.append(item);
          }
        }

        function selected() {
          if ($scope.selected != null) {
            $element.attr("selected-item", $scope.selected);
            aItem.text($scope.items[$scope.selected]);

            select.find("option").removeAttr("selected");
            angular.element(select[0].querySelector("option[value='" + $scope.selected + "']")).attr("selected", "selected");

            $element.removeClass("opened");

            if (typeof $scope.selectFn != "undefined") {
              $scope.selectFn($scope.name, $scope.selected);
            }

            if (typeof $scope.bind != "undefined") {
              $scope.bind = $scope.selected;
            }
          }
        }

        aItem.on("click", function (event) {
          $element.toggleClass("opened");
        });

        $attrs.$observe("selectedItem", function (newValue) {
          $scope.selected = newValue;
          selected();
        });

        render();
      },
      scope: {
        items: "=",
        selectFn: "=",
        name: "@",
      },
      template: "<select name=''></select><div class='selected-item'></div><div class='items'></div>"
    }
  });

  app.directive("ilHourChart", function () {
    return {
      restrict: "E",
      scope: {
        data: "=",
        from: "@",
        to: "@",
        sum: "="
      },
      link: function (scope, element, attrs) {

        scope.fromColor = "#1ab2ff";
        scope.toColor = "#ed2e26";
        scope.gradient = new Rainbow;

        var rendering = false;

        function humanize(num) {
          var ret = "";

          var i = 0;
          for (; num >= 1000; i++) {
            num = Math.round(num / 100) / 10;
          }

          ret = num;
          switch (i) {
            case 1:
              ret += "k";
              break;
            case 2:
              ret += "M";
              break;
            case 3:
              ret += "G";
              break;
            case 4:
              ret += "T";
              break;
          }

          return ret;
        }

        function getMaxValue() {
          var max = 0;

          for (var item in scope.data) {
            if (scope.data[item] > max)
              max = scope.data[item];
          }

          return max;
        }

        function toDate(str) {
          if (typeof str != "string")
            return str;

          var parts = str.replace(" ", "").split("-").map(function (i) {
            return parseInt(i)
          });

          var date = new Date();
          for (var i = 0; i < parts.length; i++) {
            switch (i) {
              case 0:
                date.setFullYear(parts[i]);
                break;
              case 1:
                date.setMonth(parts[i] - 1);
                break;
              case 2:
                date.setDate(parts[i]);
                break;
              case 3:
                date.setHours(parts[i]);
                break;
              case 4:
                date.setMinutes(parts[i]);
                break;
            }
          }

          return date;
        }

        function normalizeDate() {
          for (var item in scope.data) {
            var date = toDate(item);
            var value = scope.data[item];
            scope.normalized[date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getHours()] = value;
          }
        }

        function render() {
          rendering = true;
          scope.normalized = {};
          normalizeDate();

          scope.minValue = 0;
          scope.maxValue = getMaxValue();

          if (scope.minValue == scope.maxValue)
            scope.maxValue++;

          scope.gradient.setNumberRange(scope.minValue, scope.maxValue);
          scope.gradient.setSpectrum(scope.fromColor, scope.toColor);

          scope.fromDate = toDate(attrs.from);
          scope.toDate = toDate(attrs.to);

          element.html("");

          var wrapper = angular.element('<div class="wrapper"></div>');
          var table = angular.element('<table></table>');
          var thead = angular.element('<thead><tr></tr></thead>');
          var tbody = angular.element('<tbody></tbody>');

          table.append(thead);
          table.append(tbody);

          // table heading (hours + one empty for first column + one empty for sum column)
          thead = thead.find("tr");
          thead.append("<th></th>");
          for (var i = 1; i < 25; i++) {
            thead.append("<th>" + i + "</th>");
          }

          if (scope.sum)
            thead.append("<th></th>");

          // data print
          var days = Math.ceil(Math.abs(scope.toDate.getTime() - scope.fromDate.getTime()) / (1000 * 3600 * 24)) + 1;
          var from = scope.fromDate;

          for (var i = 0; i < days; i++, from.add) {
            var row = angular.element("<tr></tr>");
            var day = from.getFullYear() + "-" + (from.getMonth() + 1) + "-" + from.getDate();

            row.append("<td>" + (from.getMonth() + 1) + "/" + ((from.getDate() > 9) ? from.getDate() : ("0" + from.getDate())) + "</td>");

            var sum = 0;
            for (var j = 1; j <= 24; j++) {
              var td = angular.element('<td><span class="data"></span></td>');
              var data = td.find("span");
              data.text(scope.minValue);

              if (typeof scope.normalized[day + "-" + j] != "undefined") {
                td.append('<span class="bubble">' + scope.normalized[day + "-" + j] + '</span>');
                data.text(humanize(scope.normalized[day + "-" + j]));
                data[0].style.backgroundColor = "#" + scope.gradient.colourAt(scope.normalized[day + "-" + j]);
                sum += scope.normalized[day + "-" + j];
              } else {
                td.append('<span class="bubble">' + scope.minValue + '</span>');
                data[0].style.backgroundColor = "#" + scope.gradient.colourAt(scope.minValue);
              }
              row.append(td);
            }

            if (scope.sum)
              row.append('<td><span class="sum">' + humanize(sum) + '</span><span class="bubble">' + sum + '</span></td>');

            tbody.append(row);
            from.setDate(from.getDate() + 1);
          }

          var gradientBar = angular.element('<div class="gradient-bar"></div>');
          for (var i = scope.minValue; i < scope.maxValue; i += ((scope.maxValue - scope.minValue) / 9)) {
            var bar = angular.element('<div class="bar"></div>');
            bar[0].style.backgroundColor = "#" + scope.gradient.colourAt(i);
            gradientBar.append(bar);
          }
          gradientBar.append('<span class="min">' + humanize(scope.minValue) + '</span>');
          gradientBar.append('<span class="max">' + humanize(scope.maxValue) + '</span>');

          wrapper.append(table);
          wrapper.append(gradientBar);
          element.append(wrapper);
          rendering = false;
        }

        scope.$watch("data", function () {
          if (!rendering)
            render();
        });

        scope.$watch("from", function () {
          if (!rendering)
            render();
        });

        scope.$watch("to", function () {
          if (!rendering)
            render();
        });
      }
    }
  });

  app.directive("ilToggle", function () {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        element.on("click", function (event) {
          event.preventDefault();
          angular.element(document.querySelectorAll(attrs.ilToggle)).toggleClass(attrs.ilToggleClass);
        });
      }
    }
  });

  app.directive("ilLineChart", function () {
    return {
      restrict: "E",
      scope: {
        data: "=",
        labels: "=",
        height: "@",
        width: "@",
        unit: "@",
        min: "@",
        max: "@"
      },
      link: function (scope, element, attrs) {
        var canvas = angular.element(element[0].querySelector("canvas"));
        var ctx = canvas[0].getContext("2d");
        var colors = ["#19b4ff", "#ff7a5c", "#f1c925"];
        var fontSize = 15;

        function setCanvas() {
          canvas.attr("height", scope.height);
          canvas.attr("width", scope.width);
        }

        function drawMatrix(padding) {

          // Base lines (left and bottom)
          ctx.strokeStyle = "#87959E";
          ctx.lineWidth = 3;
          ctx.beginPath();
          ctx.moveTo(padding.left, padding.top);
          ctx.lineTo(padding.left, scope.height - padding.bottom);
          ctx.lineTo(scope.width - padding.right, scope.height - padding.bottom);
          ctx.stroke();
          ctx.closePath();

          // Backgrounds
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          ctx.fillStyle = "#EDEEF0";
          for (var i = 0; i < 24; i++) {
            if (i % 2 != 0) {
              ctx.fillRect(padding.left + 3 + i * part, padding.top, part, scope.height - padding.top - padding.bottom - 3);
            }
          }

          // Dividing lines
          part = (scope.height - padding.top - padding.bottom - 3) / 10;
          ctx.setLineDash([4, 3]);
          ctx.strokeStyle = "#d6dbde";
          ctx.lineWidth = 1;
          ctx.beginPath();
          for (var i = 1; i < 10; i++) {
            ctx.moveTo(padding.left + 3, padding.top + i * part);
            ctx.lineTo(scope.width - padding.right, padding.top + i * part);
          }
          ctx.stroke();
          ctx.closePath();
          ctx.setLineDash([0, 0]);
        }

        function writeValues(textPadding, padding) {

          // Unit
          ctx.font = "15px Gotham";
          ctx.fillStyle = "#87959e";
          ctx.textAlign = "end";
          ctx.fillText(scope.unit, padding.left - textPadding, padding.top - textPadding);

          // Days
          ctx.textAlign = "center";
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          for (var i = 0; i < 24; i++) {
            ctx.fillText(i + 1, padding.left + i * part + part / 2, padding.top - textPadding);
          }

          // Values
          ctx.textAlign = "end";
          ctx.textBaseline = "middle";
          part = (scope.height - padding.top - padding.bottom - 3) / 10;
          var unit = (scope.max - scope.min) / 10;
          for (var i = 0; i < 10; i++) {
            ctx.fillText(unit * ( i + 1), padding.left - textPadding, scope.height - padding.bottom - 3 - part * i - part / 2);
          }
        }

        function renderLine(data, i, padding) {
          var part = (scope.width - padding.left - padding.right - 3) / 24;
          var keys = [];
          var pixelRate = (scope.height - padding.top - padding.bottom - 3) / (scope.max - scope.min);
          for (var key in data) {
            keys.push(parseFloat(key));
          }
          keys.sort(function (a, b) {
            return a - b
          });

          ctx.strokeStyle = colors[i];
          ctx.lineWidth = 2;
          ctx.beginPath();
          ctx.moveTo(padding.left + 3 + keys[0] * part, scope.height - padding.bottom - 3 - (pixelRate) * data[keys[0]]);
          for (var i = 1; i < keys.length; i++) {
            ctx.lineTo(padding.left + 3 + keys[i] * part, scope.height - padding.bottom - 3 - (pixelRate) * data[keys[i]]);
          }
          ctx.stroke();
          ctx.closePath();
        }

        function renderLabels(textPadding, padding) {
          if (typeof scope.labels != "undefined") {
            var pWidth = 0;
            ctx.textAlign = "left";
            ctx.textBaseline = "hanging";
            for (var i = 0; i < scope.labels.length; i++) {
              ctx.fillStyle = colors[i % colors.length];
              ctx.fillRect(padding.left + pWidth - 3, scope.height - padding.top + textPadding.top, fontSize, fontSize);
              ctx.fillText(scope.labels[i], padding.left + pWidth - 3 + fontSize * 2, scope.height - padding.top + textPadding.top);
              pWidth += fontSize + textPadding.left + ctx.measureText(scope.labels[i]).width + fontSize * 2;
            }
          }
        }

        function renderLines(padding) {
          for (var i = 0; i < scope.data.length; i++) {
            renderLine(scope.data[i], i % colors.length, padding);
          }
        }

        function render() {
          var padding = {
            left: 60, top: 60, right: 0, bottom: 60
          };
          setCanvas();
          drawMatrix(padding);
          writeValues(5, padding);
          renderLines(padding);
          renderLabels({left: 10, top: 15}, padding);
        }

        render();
        scope.$watch("data", function () {
          render();
        });

        render();
        scope.$watch("labels", function () {
          render();
        })
      },
      template: "<canvas></canvas>"
    }
  });


  app.directive("ilDonutChart", function () {
    return {
      restriction: "E",
      scope: {
        data: "=",
        hoverFn: "=",
        height: "@",
        width: "@"
      },
      link: function (scope, element, attrs) {

      },
      template: "<canvas></canvas>"
    }
  });

})();
