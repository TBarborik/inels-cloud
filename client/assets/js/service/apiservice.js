angular.module("Api", ['ngCookies']).factory("ApiService", ['$http', '$cookies', function ($http, $cookies) {
  var server = "https://www.elkoep.cloud/api/v1";
  var ApiService = {};

  ApiService.versions = [
    "https://www.elkoep.cloud/api/v1",
    "https://www.elkoep.cloud/api/v2"
  ];

  ApiService.APIv1 = 0;
  ApiService.APIv2 = 1;
  ApiService.currentApi = ApiService.versions[ApiService.APIv2]; // default

  ApiService.setApi = function(v) {
    ApiService.currentApi = ApiService.versions[v];
  };

  ApiService.request = function (method, endpoint, data, successFn, errorFn) {

    var vars = {
      method: method,
      url: ApiService.currentApi + '/' + endpoint,
      data: data
    };

    if (typeof successFn != "undefined") {
      if (typeof errorFn == "undefined") {
        $http(vars).then(successFn);
      } else {
        $http(vars).then(successFn, function (r) {
            errorFn(r)
          }
        );
      }
    } else {
      $http(vars);
    }
  };

  ApiService.post = function (endpoint, data, successFn, failFn) {
    ApiService.request("POST", endpoint, data, successFn, failFn);
  };

  ApiService.get = function (endpoint, data, successFn, failFn) {
    if (typeof data === 'function') {
      successFn = data;
      failFn = successFn;
      data = {};
    }

    ApiService.request("GET", endpoint, data, successFn, failFn);
  };

  ApiService.put = function (endpoint, data, successFn, failFn) {
    if (typeof data === 'function') {
      successFn = data;
      failFn = successFn;
      data = {};
    }

    ApiService.request("PUT", endpoint, data, successFn, failFn);
  };

  ApiService.delete = function (endpoint, data, successFn, failFn) {
    ApiService.request("DELETE", endpoint, data, successFn, failFn);
  };

  return ApiService;
}]);
