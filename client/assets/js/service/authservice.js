angular.module("Authentication", ["Api", "ngCookies"]).factory("AuthService", ['ApiService', '$cookies', "$state", function (ApiService, $cookies, $state) {
  var AuthService = {};

  AuthService.isLoggedIn = function () {
    var is = typeof $cookies.get("TWISTED_SESSION") != "undefined" || (typeof(Storage) !== "undefined" && typeof(localStorage.loggedIn) !== "undefined" && localStorage.loggedIn);
    if (!is && typeof Storage !== "undefined" && typeof localStorage.username !== "undefined")
      localStorage.removeItem("username");

    return is;
  };

  AuthService.logIn = function (name, password, success, fail) {
    ApiService.setApi(ApiService.APIv1);
    ApiService.post("login", {username: name, password: password},
      function (response) {
        if (response.status == 200) {
          if (typeof(Storage) !== "undefined") {
            localStorage.username = name;
            localStorage.loggedIn = true;
          }
          success();
        }
        else
          fail();
      },
      function (response) {
        fail();
      });
    ApiService.setApi(ApiService.APIv2);
  };

  AuthService.logOut = function (success, fail) {
    ApiService.setApi(ApiService.APIv1);
    ApiService.get("signout", {}, function (response) {
      $cookies.remove("TWISTED_SESSION");
      if (typeof(Storage) !== "undefined") {
        localStorage.removeItem("username");
        localStorage.removeItem("loggedIn");
      }
      window.location = "/";
      success();
    }, function (response) {
      fail();
    });
    ApiService.setApi(ApiService.APIv2);
  };

  AuthService.getUser = function () {
    var user = {
      username: ""
    };

    if (typeof(Storage) !== "undefined" && typeof(localStorage.username) !== "undefined") {
      user.username = localStorage.username;
    }

    return user;
  };

  return AuthService;
}]);
