angular.module("Rooms", ["Api"]).factory("RoomService", ['ApiService', function (ApiService) {
  var RoomService = {};

  RoomService.devices = [
    'rgba',
    'light_switch',
    'light_dimmer',
    'blinds'
  ];

  RoomService.actual = 0;

  RoomService.getRooms = function (success, fail) {
    RoomService.getMac(RoomService.actual, function (mac) {
      ApiService.get("rooms/" + mac, function (r) {
        success(r, mac)
      }, fail);
    });
  };

  RoomService.get = function (findmac, room, success, fail) {
    if (findmac) {
      RoomService.getMac(RoomService.actual, function (mac) {
        ApiService.get("rooms/" + mac + "/" + room, function (r) {
          var data = [];
          for (var deviceType in r.data.dev_type) {
            for (var deviceOrder in r.data.dev_type[deviceType]) {
              var device = r.data.dev_type[deviceType][deviceOrder];
              device.order = parseInt(deviceOrder);
              device.type = deviceType;
              var roomName = room.split('/').pop();
              device.room = roomName;
              if (eval("typeof(RoomService." + device.type + "_in_edit)") === "function") {
                device = eval("RoomService." + device.type + "_in_edit(device)");
              }

              data.push(device);
            }
          }
          success(data, room);
        }, fail);
      });
    } else {
      ApiService.get("rooms/" + room, function (r) {
        var data = [];
        var device = {};
        for (var deviceType in r.data.dev_type) {
          for (var deviceOrder in r.data.dev_type[deviceType]) {
            device = {};
            device = r.data.dev_type[deviceType][deviceOrder];
            device.order = parseInt(deviceOrder);
            device.type = deviceType;
            var roomName = room.split('/').pop();
            device.room = roomName;
            if (eval("typeof(RoomService." + device.type + "_in_edit)") === "function") {
              device = eval("RoomService." + device.type + "_in_edit(device)");
            }
            data.push(device);
          }
        }
        success(data, room);
      }, fail);
    }
  };

  RoomService.getRoomsDevices = function (state, success, fail) {
    if (typeof(state) === "undefined") {
      state = success;
      success = fail;
    }

    RoomService.getRooms(function (response, mac) {

      // Rooms obtained, let's iterate
      for (var key in response.data) {


        RoomService.get(false, mac + "/" + response.data[key], function (devices, room) {
          var devicesArray = [];

          // Devices obtained
          for (var key2 in devices) {
            devices[key2].room = room;
            if (typeof state === "undefined" || typeof(state) === "function" || (typeof state != "undefined" && devices[key2].state == state)) {
              devicesArray.push(devices[key2]);
            }
          }

          if (typeof(state) === "function") {
            state(devicesArray)
          } else
            success(devicesArray);
        });

      }
    });
  };

  RoomService.getStatusDevices = function (success, fail) {

    ApiService.get("devices", success, fail);

  };

  RoomService.getMac = function (no, success, fail) {

    RoomService.getStatusDevices(function (r) {
      if (Object.keys(r.data).length > 0) {
        success(r.data[no].MAC);
      }
    });
  };

  RoomService.change = function (address, data, success, fail) {
    RoomService.getMac(RoomService.actual, function (mac) {
      ApiService.get("rooms/" + mac, function (r) {
        ApiService.put("devices/" + mac + "/" + address + "/action", data, success, fail);
      }, fail);
    });
  };

  RoomService.alter = function (array, data) {
    var search = "address";

    for (var i = 0; i < array.length; i++) {
      if (typeof(array[i][search]) === "undefined") {
        for (var j = 0; j < RoomService.devices.length; j++) {
          if (eval("RoomService." + RoomService.devices[j] + "_update(array, data)")) { // successfully updated means true
            return array;
          }
        }
      } else if (array[i][search] == data[search]) {
        angular.extend(array[i], data);
        return array;
      }
    }

    return array;
  };

  // Device methods

  RoomService.rgba_in_edit = function (data) {
    data.colors = {
      r: data.states.R * 2.55,
      g: data.states.G * 2.55,
      b: data.states.B * 2.55
    };

    data.states.A_old = (data.states.A) ? data.states.A : 100;

    return data;
  };

  RoomService.light_switch_update = function (array, data) {
    return false; // solved with normal method of search, therefore nothing can happen
  };

  RoomService.light_dimmer_update = function (array, data) {
    return false; // solved with normal method of search, therefore nothing can happen
  };

  // Updates blinds states (up and down)
  RoomService.blinds_update = function (array, data) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].type != "blinds")
        continue;

      var adr_name, found = false;
      for (adr_name in array[i].addresses) {
        if (array[i]["addresses"][adr_name] == data.address) {
          found = true;
          break;
        }
      }

      if (found) {
        array[i]["states"][adr_name] = data.state;
        return true;
      }
    }

    return false;
  };

  // Updates rgba states
  RoomService.rgba_update = function (array, data) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].type != "rgba")
        continue;

      var adr_name, found = false;
      for (adr_name in array[i].addresses) {
        if (array[i]["addresses"][adr_name] == data.address) {
          found = true;
          break;
        }
      }

      if (found) {
        array[i]["states"][adr_name] = data.state;
        if (adr_name != "A")
          array[i]["colors"][adr_name.toLowerCase()] = data.state * 2.55;
        return true;
      }
    }

    return false;
  };

  return RoomService;
}]);
